from ._utils import get_text


profiles = {}

profiles['Document'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Document", "en"),
                get_text("పత్రము", "te"),
            ],
            "label_plural": [
                get_text("Documents", "en"),
                get_text("పత్రములు", "te"),
            ]
        },
        "instance": {
            "name": "title",
            "by": "author",
            "description": "description",
            "img_repr": "logo",
        }
    },
    "nav": {
        "source": [
            {
                "json_class": "DocumentSection",
                "default_expanded": True,
                "label": [
                    get_text("Sections", "en"),
                    get_text("విభాగాలు", "te"),
                ]
            },
            {
                "json_class": "ImageAsset",
                "default_expanded": False,
                "label": [
                    get_text("Image Assets", "en"),
                    get_text("చిత్ర వనరులు", "te"),
                ]
            },
            {
                "json_class": "WorkLog",
                "label": [
                    get_text("Work logs", "en"),
                    get_text("కార్య చిట్టా", "te"),
                ],
            },
        ],
    },
    "actions": [],
    "search": {
        "simple": {
            "fields": ["title", "description"],
            "label": [
                get_text("name or description", "en"),
                get_text("పేరు లేదా వివరణ", "te"),
                get_text("नाम, विवरणम् वा", "sa"),
            ],
        },
        "advanced": [
            {
                "field": "title",
                "label": [
                    get_text("Title", "en"),
                    get_text("పేరు", "te"),
                    get_text("नाम", "sa"),
                ],
            },
            {
                "field": "description",
                "label": [
                    get_text("Description", "en"),
                    get_text("వివరణ", "te"),
                    get_text("विवरणम्", "sa"),
                ]
            },
        ],
    },
}

profiles['DocumentSection'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Document Section", "en"),
                get_text("పత్ర విభాగం", "te"),
            ],
            "label_plural": [
                get_text("Document Sections", "en"),
                get_text("పత్ర విభాగాలు", "te"),
            ]
        },
        "instance": {
            "name": "title",
            "description": "description",
            "img_repr": "effective_img_parts.0.source",
            "img_repr_selector": "effective_img_parts.0.selector",
        }
    },
    "nav": {
        "source": [
            {
                "json_class": "DocumentSection",
                "default_expanded": True,
                "label": [
                    get_text("Sections", "en"),
                    get_text("విభాగాలు", "te"),
                ]
            },
            {
                "json_class": "ImageAsset",
                "default_expanded": False,
                "label": [
                    get_text("Image Assets", "en"),
                    get_text("చిత్ర వనరులు", "te"),
                ]
            },
            {
                "json_class": "WorkLog",
                "label": [
                    get_text("Work logs", "en"),
                    get_text("కార్య చిట్టా", "te"),
                ],
            },
        ],
    },
    "actions": [],
    "search": {
        "simple": {
            "fields": ["title", "description"],
            "label": [
                get_text("name or description", "en"),
                get_text("పేరు లేదా వివరణ", "te"),
                get_text("नाम, विवरणम् वा", "sa"),
            ],
        },
        "advanced": [
            {
                "field": "title",
                "label": [
                    get_text("Title", "en"),
                    get_text("పేరు", "te"),
                    get_text("नाम", "sa"),
                ],
            },
            {
                "field": "description",
                "label": [
                    get_text("Description", "en"),
                    get_text("వివరణ", "te"),
                    get_text("विवरणम्", "sa"),
                ]
            },
        ],
    },
}


profiles['WorkLog'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Work Log", "en"),
                get_text("కార్య చిట్టా", "te"),
            ],
            "label_plural": [
                get_text("Work Logs", "en"),
                get_text("కార్య చిట్టాలు", "te"),
            ]
        },
        "instance": {
            "name": "title",
            "description": "description",
        }
    },
    "nav": {},
    "actions": [],
    "search": {
        "simple": {
            "fields": ["title", "description"],
            "label": [
                get_text("name or description", "en"),
                get_text("పేరు లేదా వివరణ", "te"),
                get_text("नाम, विवरणम् वा", "sa"),
            ],
        },
        "advanced": [
            {
                "field": "title",
                "label": [
                    get_text("Title", "en"),
                    get_text("పేరు", "te"),
                    get_text("नाम", "sa"),
                ],
            },
            {
                "field": "description",
                "label": [
                    get_text("Description", "en"),
                    get_text("వివరణ", "te"),
                    get_text("विवरणम्", "sa"),
                ]
            },
        ],
    },
}


profiles['ImageAsset'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Image Asset", "en"),
                get_text("చిత్ర వనరు", "te"),
            ],
            "label_plural": [
                get_text("Image Assets", "en"),
                get_text("చిత్ర వనరులు", "te"),
            ]
        },
        "instance": {
            "img_repr": "source_img",
            "img_repr_selector": "image_selector",
            "name": "title"
        }
    },
    "nav": {},
    "actions": [],
    "search": {
        "simple": {
            "fields": ["title", "description"],
            "label": [
                get_text("name or description", "en"),
                get_text("పేరు లేదా వివరణ", "te"),
                get_text("नाम, विवरणम् वा", "sa"),
            ],
        },
        "advanced": [
            {
                "field": "source",
                "label": [
                    get_text("Parent resource", "en"),
                    get_text("మాతృ వనరు", "te"),
                ]
            },
            {
                "field": "title",
                "label": [
                    get_text("Title", "en"),
                    get_text("పేరు", "te"),
                    get_text("नाम", "sa"),
                ],
            },
            {
                "field": "description",
                "label": [
                    get_text("Description", "en"),
                    get_text("వివరణ", "te"),
                    get_text("विवरणम्", "sa"),
                ]
            },
        ],
    },
}
