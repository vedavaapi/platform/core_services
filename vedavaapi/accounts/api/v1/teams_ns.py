import json

from flask import g
import flask_restx
import six
from jsonschema import ValidationError
from vedavaapi.common.helpers.args_parse_helper import parse_json_args

from vedavaapi.acls.permissions_helper import PermissionResolver
from vedavaapi.objectdb.helpers import ObjModelException, projection_helper, objstore_helper

from vedavaapi.common.helpers.api_helper import jsonify_argument, check_argument_type, error_response, abort_with_error_response
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from . import api
from .users_ns import get_requested_agents, _validate_projection
from ...agents_helpers import teams_helper

teams_ns = api.namespace('teams', path='/teams', description='teams namespace')


@teams_ns.route('')
class Teams(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument('selector_doc', location='args', type=str, default='{}')
    get_parser.add_argument('projection', location='args', type=str)
    get_parser.add_argument('start', location='args', type=int)
    get_parser.add_argument('count', location='args', type=int)
    get_parser.add_argument('sort_doc', location='args', type=str)
    get_parser.add_argument('return_count', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    post_parser = api.parser()
    post_parser.add_argument('team_json', location='form', type=str, required=True)
    post_parser.add_argument('return_projection', location='form', type=str)
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_parser = api.parser()
    delete_parser.add_argument('team_ids', location='form', type=str, required=True)
    delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @teams_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):
        args = self.get_parser.parse_args()

        response_json = get_requested_agents(
            args, g.users_colln,
            current_token.user_id, current_token.team_ids,  filter_doc={"jsonClass": "Team"})

        should_return_count = json.loads(args.get('return_count', 'false'))
        if should_return_count:
            return {"count": len(response_json['items'])}, 200

        return response_json, 200

    @teams_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = self.post_parser.parse_args()

        team_json = jsonify_argument(args['team_json'], key='team_json')
        check_argument_type(team_json, (dict,), key='team_json')

        return_projection = jsonify_argument(args.get('return_projection', None), key='return_projection')
        check_argument_type(return_projection, (dict,), key='return_projection', allow_none=True)
        _validate_projection(return_projection)
        return_projection = projection_helper.modified_projection(
            return_projection, mandatory_attrs=['_id', 'jsonClass'])

        try:
            new_team_id = teams_helper.create_new_team(
                g.users_colln, g.acl_svc, team_json,
                current_token.user_id, current_token.team_ids, g.schema_validator, initial_agents=g.initial_agents)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except ValidationError as e:
            return error_response(message='invalid schema for team_json', code=403, details={"error": str(e)})

        new_team_json = g.users_colln.get(new_team_id, projection=return_projection)
        return new_team_json, 200

    @teams_ns.expect(delete_parser, validate=True)
    @require_oauth()
    def delete(self):
        args = self.delete_parser.parse_args()

        team_ids = jsonify_argument(args['team_ids'])
        check_argument_type(team_ids, (list,))

        ids_validity = False not in [isinstance(_id, str) for _id in team_ids]
        if not ids_validity:
            return error_response(message='ids should be strings', code=404)

        delete_report = []

        for team_id in team_ids:
            deleted, deleted_res_ids = objstore_helper.delete_tree(
                g.users_colln, g.acl_svc, None, team_id, current_token.user_id, current_token.team_ids)

            delete_report.append({
                "deleted": deleted,
                "deleted_resource_ids": deleted_res_ids
            })

        return delete_report, 200


@teams_ns.route('/<team_identifier>')
class TeamResource(flask_restx.Resource):

    get_parser = teams_ns.parser()
    get_parser.add_argument('identifier_type', type=str, location='args', default='_id', choices=['_id', 'name'])
    get_parser.add_argument('projection', type=str, location='args')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    post_parser = teams_ns.parser()
    post_parser.add_argument('identifier_type', type=str, location='args', default='_id', choices=['_id', 'name'])
    post_parser.add_argument('update_doc', type=str, location='form', required=True)
    post_parser.add_argument('return_projection', type=str, location='form')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @staticmethod
    def _team_selector_doc(team_identifier, identifier_type):
        if identifier_type not in ('_id', 'name'):
            error = error_response(message='invalid identifier_type', code=400)
            abort_with_error_response(error)
        return {
            "_id": teams_helper.get_team_selector_doc(_id=team_identifier),
            "name": teams_helper.get_team_selector_doc(team_name=team_identifier)
        }.get(identifier_type)

    @teams_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, team_identifier):
        args = self.get_parser.parse_args()

        identifier_type = args.get('identifier_type', '_id')
        team_selector_doc = self._team_selector_doc(team_identifier, identifier_type)

        projection = jsonify_argument(args.get('projection', None), key='projection')
        check_argument_type(projection, (dict,), key='projection', allow_none=True)
        _validate_projection(projection)
        projection = projection_helper.modified_projection(projection, mandatory_attrs=["_id", "jsonClass"])

        try:
            team_json = objstore_helper.get_resource(
                g.users_colln, g.acl_svc, team_selector_doc, current_token.user_id, current_token.team_ids)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        if not team_json['resolvedPermissions'][PermissionResolver.READ]:
            return error_response(message='permission denied', code=403)

        projected_team_json = projection_helper.project_doc(team_json, projection)
        return projected_team_json, 200

    @teams_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self, team_identifier):
        args = self.post_parser.parse_args()

        identifier_type = args.get('identifier_type', '_id')
        team_selector_doc = self._team_selector_doc(team_identifier, identifier_type)

        update_doc = jsonify_argument(args['update_doc'], key='update_doc')
        check_argument_type(update_doc, (dict,), key='update_doc')

        if 'jsonClass' not in update_doc:
            update_doc['jsonClass'] = 'Team'
        if update_doc['jsonClass'] != 'Team':
            return error_response(message='invalid jsonClass', code=403)

        if identifier_type == '_id':
            update_doc.pop('name', None)
            if update_doc['_id'] != team_identifier:
                return error_response(message='invalid user_id', code=403)
        else:
            team_id = teams_helper.get_team_id(g.users_colln, team_identifier)
            if not team_id:
                return error_response(message='no team with team_name {}'.format(team_identifier))
            update_doc['_id'] = team_id

        return_projection = jsonify_argument(args.get('return_projection', None), key='return_projection')
        check_argument_type(return_projection, (dict,), key='return_projection', allow_none=True)
        _validate_projection(return_projection)
        return_projection = projection_helper.modified_projection(
            return_projection, mandatory_attrs=['_id', 'jsonClass'])

        try:
            updated_team_id = objstore_helper.update_resource(
                g.users_colln, g.acl_svc, update_doc, current_token.user_id, current_token.team_ids, g.schema_validator,
                not_allowed_attributes=['members', 'name'])
            if updated_team_id is None:
                raise ObjModelException('team not exist', 404)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except ValueError as e:
            return error_response(message='schema validation error', code=403, details={"error": str(e)})

        team_json = g.users_colln.find_one(team_selector_doc, projection=return_projection)
        return team_json, 200


@teams_ns.route('/<team_identifier>/members')
class Members(flask_restx.Resource):

    get_parser = teams_ns.parser()
    get_parser.add_argument('identifier_type', type=str, location='args', default='_id', choices=['_id', 'name'])
    get_parser.add_argument('return_count', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument('only_explicit_members', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument('return_resources', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument('projection', location='args', type=str)
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    get_payload_json_parse_directives = {
        "projection": {
            "allowed_types": (dict,), "allow_none": True, "custom_validator": _validate_projection
        },
    }


    post_parser = teams_ns.parser()
    post_parser.add_argument('identifier_type', type=str, location='args', default='_id', choices=['_id', 'name'])
    post_parser.add_argument('member_identifiers', type=str, location='form', required=True)
    post_parser.add_argument('member_identifiers_type', type=str, location='form', choices=['_id', 'email'], default='_id')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_payload_json_parse_directives = {
        "member_identifiers": {"allowed_types": (list, ), "allow_none": False}
    }

    delete_parser = teams_ns.parser()
    delete_parser.add_argument(
        'identifier_type', type=str, location='args', default='_id', choices=['_id', 'name'])
    delete_parser.add_argument('member_identifiers', type=str, location='form', required=True)
    delete_parser.add_argument('member_identifiers_type', type=str, location='form', choices=['_id', 'email'], default='_id')
    delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_payload_json_parse_directives = {
        "member_identifiers": {"allowed_types": (list, ), "allow_none": False}
    }

    @teams_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, team_identifier):
        args = parse_json_args(self.get_parser.parse_args(), self.get_payload_json_parse_directives)

        identifier_type = args.get('identifier_type', '_id')
        # noinspection PyProtectedMember
        team_selector_doc = TeamResource._team_selector_doc(team_identifier, identifier_type)

        team_json = g.users_colln.find_one(team_selector_doc, projection=None)
        if team_json is None:
            return error_response(message='team not found', code=404)

        if not PermissionResolver.resolve_permission(
                team_json, PermissionResolver.READ,
                current_token.user_id, current_token.team_ids, g.users_colln, g.acl_svc):
            return error_response(message='permission denied', code=403)

        team_id = team_json['_id']
        only_explicit_members = json.loads(args.get('only_explicit_members', 'false'))
        member_ids = teams_helper.get_team_members(g.users_colln, team_id, only_explicit_members=only_explicit_members)

        if args.get('return_count', 'false') == 'true':
            return {"count": len(member_ids)}
        if args.get('return_resources', 'false') == 'true':
            return objstore_helper.get_read_permitted_resources(
                g.users_colln, g.acl_svc, current_token.user_id, current_token.team_ids,
                {"jsonClass": "User", "_id": {"$in": member_ids}}, projection=args['projection']
            )
        return member_ids

    @teams_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self, team_identifier):
        args = parse_json_args(self.post_parser.parse_args(), self.post_payload_json_parse_directives)

        identifier_type = args.get('identifier_type', '_id')
        # noinspection PyProtectedMember
        team_selector_doc = TeamResource._team_selector_doc(team_identifier, identifier_type)

        user_identifiers = args['member_identifiers']
        for user_identifier in user_identifiers:
            if not isinstance(user_identifier, six.string_types):
                return error_response(message='invalid member_identifiers', code=400)

        try:
            # noinspection PyUnusedLocal
            modified_count = teams_helper.add_users_to_team(
                g.users_colln, g.acl_svc, team_selector_doc, user_identifiers,
                current_token.user_id, current_token.team_ids, user_identifiers_type=args['member_identifiers_type'])
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        member_ids = g.users_colln.find_one(team_selector_doc, projection={"members": 1}).get('members', [])
        return member_ids, 200

    @teams_ns.expect(delete_parser, validate=True)
    @require_oauth()
    def delete(self, team_identifier):
        args = parse_json_args(self.delete_parser.parse_args(), self.delete_payload_json_parse_directives)

        identifier_type = args.get('identifier_type', '_id')
        # noinspection PyProtectedMember
        team_selector_doc = TeamResource._team_selector_doc(team_identifier, identifier_type)

        user_identifiers = args['member_identifiers']
        for user_identifier in user_identifiers:
            if not isinstance(user_identifier, six.string_types):
                return error_response(message='invalid user_ids', code=400)

        try:
            # noinspection PyUnusedLocal
            modified_count = teams_helper.remove_users_from_team(
                g.users_colln, g.acl_svc, team_selector_doc, user_identifiers,
                current_token.user_id, current_token.team_ids, user_identifiers_type=args['member_identifiers_type'])
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        member_ids = g.users_colln.find_one(team_selector_doc, projection={"members": 1}).get('members', [])
        return member_ids, 200
