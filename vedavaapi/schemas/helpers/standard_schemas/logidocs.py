schemas = [
    {
        "jsonClass": "VVSchema",
        "name": "DocTypeMeroSpec",
        "source": "Resource",
        "properties": {
            "name": {"type": "string"},
            "label": {
                "type": "array",
                "items": {
                    "type": "object",
                    "allowedClasses": ["Text"]
                }
            },
            "is_pseudo": {
                "type": "boolean"
            },
            "mero_specs": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "holonym_cls": {"type": "str"},
                        "meronymy_type": {
                            "enum": ["part-of", "attr-of"]
                        },
                    },
                    "required": ["holonym_cls", "meronymy_type"],
                },
            },
        },
        "required": ["name", "mero_specs"]
    },
    {
        "jsonClass": "VVSchema",
        "name": "DocumentSection",
        "source": "Resource",
        "properties": {
            "title": {"type": "string"},
            "description": {"type": "string"},
            "index": {"type": "number"},
            "content": {
                "type": "object",
                "allowedClasses": ["Text"],
            },
            "section_type": {
                "enum": ["major", "pseudo_major", "minor"]
            },
            "is_virtual_mero_top": {"type": "bool", "default": False},  # for atoms
            "source_page_ids": {
                # for major section
                "type": "array",
                "items": {"type": "string"},
            },
            "asset_pages_range": {
                # for major section
                "type": "array",
                "items": "number",
                "minItems": 2,
                "maxItems": 2,
            },
            "effective_img_parts": {
                # for atom section
                "type": "array",
                "items": {
                    "type": "object",
                    "source": {"type": "string"},
                    "selector": {
                        "type": "object",
                        "allowedClasses": ["FragmentSelector"]
                    }
                }
            }
        },
        "required": ["section_type"],
        "_primary_key": [["source", "index"], ["canonical"]],
    },
    {
        "jsonClass": "VVSchema",
        "name": "Document",
        "source": "Resource",
        "properties": {
            "title": {"type": "string"},
            "author": {
                "type": "array",
                "items": {"type": "string"}
            },
            "description": {"type": "string"},
            "meronomy_spec": {
                "type": "object",
                "additionalProperties": {
                    "type": "object",
                    "allowedClasses": ["DocTypeMeroSpec"],
                },
            },
            "section_type": {
                "enum": ["major"]
            }
        },
        "required": ["section_type", "title"],
        "_primary_key": [["source", "title"]]
    },
    {
        "jsonClass": "VVSchema",
        "name": "ImageAsset",
        "source": "Resource",
        "properties": {
            "title": {"type": "string"},
            "description": {"type": "string"},
            "canonical": {"type": "string"},
            "source_canvas": {"type": "string"},
            "source_index": {"type": "number"},
            "source_img": {"type": "string"},
            "img_selector": {
                "type": "object",
                "allowedTypes": ["FragmentSelector"],
            },
        },
        "required": ["source_img", "img_selector"],
    },
    {
        "jsonClass": "VVSchema",
        "name": "WorkLog",
        "source": "Resource",
        "properties": {
            "title": {"type": "string"},
            "assigned": {"type": "string"},
            "completed": {"type": "boolean"},
            "items": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "title": {"type": "string"},
                        "done": {"type": "boolean"},
                        "note": {"type": "string"},
                        "progress": {
                            "type": "object",
                            "properties": {
                                "current": {"type": "number"},
                                "total": {"type": "number"}
                            },
                            "required": ["current", "number"]
                        }
                    },
                    "required": ["title", "done"]
                }
            }
        },
        "required": ["title", "completed", "items"]
    }
]