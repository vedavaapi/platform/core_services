
CLASS_FIELD = 'jsonClass'

# noinspection PyDictCreation
vv_builtin_schemas = {}


vv_builtin_schemas["JsonObject"] = {
    CLASS_FIELD: "VVSchema",
    "name": "JsonObject",
    "type": "object",
    "properties": {
        CLASS_FIELD: {
            "title": CLASS_FIELD,
        },
        "type": {
            "enum": ["owl:Thing"]
        },
        "jsonClassLabel": {
            "title": "label for json class.",
            "type": "string"
        },
        "version": {
            "type": "number"
        }
    },
    "required": [CLASS_FIELD],
    "_display": []
}


vv_builtin_schemas["MetadataItem"] = {
    "name": "MetadataItem",
    "properties": {
        "type": {
            "enum": ["vv:MetadataItem"]
        },
        "label": {
            "type": "string"
        },
        "value": {
            "type": ["string", "boolean", "number", "object", "array"]
        }
    },
    "required": ["label", "value"],
    "_display": ["label", "value"],
    "source": "JsonObject"
}


vv_builtin_schemas["VVObject"] = {
    "name": "VVObject",
    "properties": {
        "type": {
            "enum": ["rdfs:Resource"]
        },
        "_id": {
            "description": "unique id of the object",
            "type": "string"
        },
        "creator": {
            "anyOf": [
                {
                    "type": "string"
                },
                {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            ]
        },
        "created": {
            "type": "string",
            "format": "date-time"
        },
        "generator": {
            "type": "string",
        },
        "modified": {
            "type": "string",
            "format": "date-time"
        },
        "metadata": {
            "type": "array",
            "items": {
                "type": "object",
                "allowedClasses": ["MetadataItem"]
            }
        },
        "resolvedPermissions": {
            "type": "object",
            "allowedClasses": ["ResolvedPermissions"]
        },
        "state": {
            "enum": ["system_inferred", "user_supplied", "user_edited", "user_confirmed", "user_rejected"]
        },
        "source": {
            "type": "string",
            "description": "id of its parent VVObject"
        },
        "target": {
            "type": "string",
            "description": "id of referred VVObject"
        },
        "is_leaf": {
            "type": "boolean",
            "description": "No referrers?"
        }
    },
    "required": ["type",],
    "_display": [],
    "_primary_keys": [],
    "source": "JsonObject"
}


locale_text_options_schema = {
    "type": "array",
    "items": {
        "type": "object",
        "allowedClasses": ["Text"],
    },
}


field_template_schema = {
    "type": "object",
    "properties": {
        "type": {
            "enum": ["field", "template"],
            "default": "field",
        },
        "value": {
            "type": "string",
        },
        "field_of": {
            "enum": ['_', '_m', 'p', 'pm', 'pr'],
            "default": "_",
        }
    },
}

field_template_choice_schema = {
    "anyOf": [
        {
            "type": "string",
            "description": "field path"
        },
        field_template_schema,
    ],
}


# TODO should use definitions

ui_repr_schema = {
    "type": "object",
    "description": "key, value pairs for semantic_property:ordered_template_choices",
    "properties": {
        "cls": {
            "type": "object",
            "properties": {
                "label": locale_text_options_schema,
                "label_plural": locale_text_options_schema,
                # "description": locale_text_options_schema,
            },
        },
        "instance": {
            "type": "object",
            "properties": {
                "name": field_template_choice_schema,
                "by": field_template_choice_schema,
                "description": field_template_choice_schema,
                "img_repr": field_template_choice_schema,
                "img_repr_selector": field_template_choice_schema,
                "txt_repr": field_template_choice_schema,
                "txt_repr_selector": field_template_choice_schema,
            },
        },
        "projection": {
            "type": "object",
            "additionalProperties": {
                "type": "number",
                "enum": [0, 1],
            },
        },
    },
    "additionalProperties": False,
}


ui_nav_link_specs_schema = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "json_class": {
                "type": "string",
            },
            "label": locale_text_options_schema,
            "default_expanded": {
                "type": "boolean",
                "default": False,
            },
        }
    }
}

ui_nav_link_specs_classified_schema = {
    "type": "object",
    "description": "field to nav specs map",
    "additionalProperties": ui_nav_link_specs_schema,
}

ui_class_actions_schema = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "id": {
                "type": "string",
            },
            "label": locale_text_options_schema,
            "params": {
                "type": "object",
                "description": "param:template_string k,v pair",
                "additionalProperties": field_template_choice_schema,
            },
        },
    },
}


ui_simple_search_profile_schema = {
    "type": "object",
    "properties": {
        "fields": {
            "type": "array",
            "items": {
                "type": "string",
            },
        },
        "label": locale_text_options_schema,
    },
}

ui_advamced_search_profile_schema = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "field": {
                "type": "string",
            },
            "label": locale_text_options_schema,
            "type": {
                "enum": ["string", "number", "_id", "bool"],
                "default": "string",
            },
            "required": {
                "type": "boolean",
                "default": False,
            }
        },
    },
}

ui_search_profile_schema = {
    "type": "object",
    "properties": {
        "simple": ui_simple_search_profile_schema,
        "advanced": ui_advamced_search_profile_schema,
    },
}

vv_builtin_schemas["UiProfile"] = {
    CLASS_FIELD: "VVSchema",
    "name": "UiProfile",
    "source": "JsonObject",
    "type": "object",
    "properties": {
        "repr": ui_repr_schema,
        "nav": ui_nav_link_specs_classified_schema,
        "actions": ui_class_actions_schema,
        "search": ui_search_profile_schema,
    }
}


vv_builtin_schemas["VVSchema"] = {
    CLASS_FIELD: "VVSchema",
    "name": "VVSchema",
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        },
        "source": {
            "type": "string"
        },
        "properties": {
            "type": "object"
        },
        "_display": {
            "type": "array",
            "items": {
                "type": "string",
            },
        },
        "_primary_keys": {
            "type": "array",
            "items": {
                "type": "array",
                "items": {
                    "type": "string",
                },
            }
        },
        "_iiif_model": {
            "type": "object",
            "implements": {
                "type": "array",
                "items": {
                    "type": "string",
                },
            },
            "data_fields": {
                "type": "object",
                "additionalProperties": {"type": "string"}
            }
        },
        "_ui_profile": {
            "type": "object",
            "allowedClasses": ["UiProfile"],
        },
    },
    "_display": ["name", "properties"],
    "_primary_keys": [["name"]],
    "source": "VVObject"
}

vv_builtin_schemas['VVSchema'].pop('required', None)

vv_builtin_schemas["Agent"] = {
    "name": "Agent",
    "properties": {
        "type": {
            "enum": ["foaf:Agent"]
        },
        "agentClass": {
            "title": "agent class",
            "type": "string",
            "enum": ["Software", "Person", "Team", "Organisation"]
        }
    },
    "_display": [],
    "source": "VVObject"
}


vv_builtin_schemas["AgentIds"] = {
    "name": "AgentIds",
    "properties": {
        "type": {
            "enum": ["foaf:Agent"]
        },
        "users": {
            "type": "array",
            "items": {
                "type": "string",
                "description": "_id of user agent"
            },
            "uniqueItems": True
        },
        "teams": {
            "type": "array",
            "items": {
                "type": "string",
                "description": "_id of team agent"
            },
            "uniqueItems": True
        }
    },
    "required": ["users", "teams"],
    "additionalProperties": False,
    "_display": ["users", "teams"],
    "source": "JsonObject"
}


vv_builtin_schemas["ActionControl"] = {
    "name": "ActionControl",
    "definitions": {
        #  "agentSet": {
        #    "type": "object",
        #    "allowedClasses": ["AgentSet"]  # TODO should able to validate classes in definitions
        #  }
        "agentSet": vv_builtin_schemas['AgentIds']
    },
    "properties": {
        "type": {
            "enum": ["vv:ActionControl"]
        },
        "grant": vv_builtin_schemas['AgentIds'],
        "revoke": vv_builtin_schemas['AgentIds'],
        "block": vv_builtin_schemas['AgentIds']
    },
    "required": ["grant", "revoke", "block"],
    "additionalProperties": False,
    "_display": ["grant", "revoke", "block"],
    "source": "JsonObject"
}

READ = "read"
UPDATE_CONTENT = "updateContent"
UPDATE_LINKS = "updateLinks"
UPDATE_PERMISSIONS = "updatePermissions"
DELETE = "delete"
CREATE_CHILDREN = "createChildren"
CREATE_ANNOS = "createAnnos"

ACTIONS = (READ, UPDATE_CONTENT, UPDATE_LINKS, UPDATE_PERMISSIONS, DELETE, CREATE_CHILDREN, CREATE_ANNOS)

vv_builtin_schemas["Acl"] = {
    "name": "Acl",
    "description": "action:permission pairs",
    "definitions": {
        #  "permission": {
        #    "type": "object",
        #    "allowedClasses": ["Permission"]
        #  }
        "permission": vv_builtin_schemas['ActionControl']
    },
    "properties": {
        "type": {
            "enum": ["vv:Acl"]
        },
        READ: vv_builtin_schemas['ActionControl'],
        UPDATE_CONTENT: vv_builtin_schemas['ActionControl'],
        UPDATE_LINKS: vv_builtin_schemas['ActionControl'],
        UPDATE_PERMISSIONS: vv_builtin_schemas['ActionControl'],
        DELETE: vv_builtin_schemas['ActionControl'],
        CREATE_CHILDREN: vv_builtin_schemas['ActionControl'],
        CREATE_ANNOS: vv_builtin_schemas['ActionControl'],
    },
    "required": list(ACTIONS),
    "additionalProperties": False,
    "_display": list(ACTIONS),
    "source": "JsonObject"
}


vv_builtin_schemas["ResolvedPermissions"] = {
    "properties": {
        "type": {
            "enum": "vv:Permissions"
        },
        READ: {"type": "boolean"},
        UPDATE_CONTENT: {"type": "boolean"},
        UPDATE_LINKS: {"type": "boolean"},
        UPDATE_PERMISSIONS: {"type": "boolean"},
        DELETE: {"type": "boolean"},
        CREATE_CHILDREN: {"type": "boolean"},
        CREATE_ANNOS: {"type": "boolean"}
    },
    "additionalProperties": False,
    "_display": list(ACTIONS),
    "source": "JsonObject",
    "name": "ResolvedPermissions"
}


vv_builtin_schemas["Selector"] = {
    "properties": {
        "type": {
            "enum": ["oa:Selector"]
        },
        "refinedBy": {
            "title": "refined by",
            "type": "object",
            "allowedClasses": ["Selector", "SelectorChoice"]
        }
    },
    "source": "JsonObject",
    "name": "Selector"
}


vv_builtin_schemas["SelectorChoice"] = {
    "properties": {
        "type": {
            "enum": ["oa:Choice"],
            "default": "oa:Choice"
        },
        "item": {
            "anyOf": [
                {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "allowedClasses": ["Selector"]
                    }
                },
                {
                    "type": "object",
                    "allowedClasses": ["Selector"]
                }
            ]
        },
        "default": {
            "type": "object",
            "allowedClasses": ["Selector"]
        }
    },
    "source": "JsonObject",
    "name": "SelectorChoice"
}


vv_builtin_schemas["Resource"] = {
    "properties": {
        "type": {
            "enum": ["oa:SpecificResource"]
        },
        "source": {
            "title": "parent",
            "description": "id of its parent resource",
            "type": "string",
        },
        "selector": {
            "type": "object",
            "allowedClasses": ["Selector", "SelectorChoice"]
        }
    },
    "_display": [],
    "name": "Resource",
    "source": "VVObject"
}

#    standard_namespaces = ('_vedavaapi', '_web')

vv_builtin_schemas["OOLDNamespaceData"] = {
    "properties": {
        "namespace": {
            "type": "string"
        }
    },
    "required": ["namespace"],
    "_display": ["namespace"],
    "name": "OOLDNamespaceData",
    "source": "JsonObject"
}


vv_builtin_schemas["VedavaapiOOLDNamespaceData"] = {
        "properties": {
            "namespace": {
                "enum": ["_vedavaapi"]
            },
            "name": {
                "type": "string"
            },
            "mimetype": {
                "type": "string"
            },
            "contentType": {
                "type": "string"
            }
        },
        "_display": ["name", "mimetype", "contentType"],
        "name": "VedavaapiOOLDNamespaceData",
        "source": "OOLDNamespaceData"
    }


vv_builtin_schemas["OOLData"] = {
    "properties": {
        "type": {
            "enum": ["owl:Thing"]
        },
        "namespace": {
            "type": "string",
        },
        "identifier": {
            "type": "string"
        },
        "url": {
            "type": "string",
            "format": "uri-reference"
        },
        "namespaceData": {
            "type": "object",
            "allowedClasses": ["OOLDNamespaceData"]
        }
    },
    "required": ["identifier", "namespace", "source"],
    "_display": ["namespace", "identifier", "url"],
    "name": "OOLData",
    "source": "VVObject"
}


vv_builtin_schemas["Annotation"] = {
    "properties": {
        "type": {
            "enum": ["oa:Annotation"]
        },
        "motivation": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "body": {
            "type": ["string", "array", "object", "number", "boolean"]
        },
        "target": {
            "anyOf": [
                {
                    "type": "string",
                    "description": "_id string"
                },
                {
                    "type": "array",
                    "items": {
                        "type": "string",
                        "description": "_id string"
                    },
                    "uniqueItems": True
                }
            ]
        },
        "targetSelector": {
            "anyOf": [
                {
                    "type": "array",
                    "items": [{
                        "type": "object",
                        "allowedClasses": ["Selector", "SelectorChoice"]
                    }]
                },
                {
                    "type": "object",
                    "allowedClasses": ["Selector", "SelectorChoice"]
                }
            ]
        }
    },
    "required": ["target"],
    "_display": ["body", "target", "jsonClassLabel", "targetSelector"],
    "name": "Annotation",
    "source": "VVObject"
}


vv_builtin_schemas["OAuth2Client"] = {
    "properties": {
        "client_id": {
            "type": "string"
        },
        "client_secret": {
            "type": "string"
        },
        "issued_at": {
            "type": "number",
            "minimum": 0
        },
        "expires_at": {
            "type": "number",
            "minimum": 0
        },
        "redirect_uris": {
            "type": "array",
            "items": {
                "type": "string",
                "format": "uri"
            }
        },
        "token_endpoint_auth_method": {
            "enum": ["none", "client_secret_post", "client_secret_basic"]
        },
        "grant_types": {
            "type": "array",
            "items": {
                "enum": ["authorization_code", "refresh_token", "implicit", "client_credentials", "password"]
            }
        },
        "response_types": {
            "type": "array",
            "items": {
                "enum": ["code", "token"]
            }
        },
        "scope": {
            "type": "string"
        },
        "name": {
            "type": "string"
        },
        "user_id": {
            "type": "string"
        }
    },
    "required": ["client_id"],
    "name": "OAuth2Client",
    "source": "Agent"
}


vv_builtin_schemas["OAuth2AuthorizationCode"] = {
    "properties": {
        "code": {
            "type": "string"
        },
        "client_id": {
            "type": "string"
        },
        "redirect_uri": {
            "type": "string"
        },
        "response_type": {
            "enum": ["code", "token"]
        },
        "scope": {
            "type": "string"
        },
        "auth_time": {
            "type": "number",
            "minimum": 0
        },
        "user_id": {
            "type": "string"
        }
    },
    "required": ["code", "client_id", "scope", "user_id", "redirect_uri"],
    "name": "OAuth2AuthorizationCode",
    "source": "JsonObject"
}


vv_builtin_schemas["OAuth2Token"] = {
    "properties": {
        "client_id": {
            "type": "string"
        },
        "token_type": {
            "enum": ["Bearer", "Mac"]
        },
        "access_token": {
            "type": "string"
        },
        "refresh_token": {
            "type": "string"
        },
        "scope": {
            "type": "string"
        },
        "revoked": {
            "type": "boolean"
        },
        "issued_at": {
            "type": "number",
            "minimum": 0
        },
        "expires_in": {
            "type": "number",
            "minimum": 0
        },
        "user_id": {
            "type": "string"
        }
    },
    "required": [],
    "name": "OAuth2Token",
    "source": "JsonObject"
}


vv_builtin_schemas["OAuth2MasterConfig"] = {
    "properties": {
        "grant_privileges": {
            "type": "object",
            "properties": {
                "client_credentials": {
                    "type": "object",
                    "allowedClasses": ["AgentSet"]
                },
                "password": {
                    "type": "object",
                    "allowedClasses": ["AgentSet"]
                }
            }
        }
    },
    "name": "OAuth2MasterConfig",
    "source": "JsonObject"
}


vv_builtin_schemas["User"] = {
    "properties": {
        "type": {
            "enum": ["foaf:Agent"]
        },
        "name": {
            "type": "string",
        },
        "email": {
            "type": "string",
            "format": "email"
        },
        "picture": {
            "type": "string",
        },
        "password": {
            "type": "string",
            "format": "password"
        },
        "visible_fields": {
            "type": "object",
            "additionalProperties": {
                "type": "number",
                "enum": [0, 1]
            }
        }
    },
    "required": ["email"],
    "_display": ["email", "name", "password"],
    "_primary_keys": [["email"]],
    "name": "User",
    "source": "Agent"
}


vv_builtin_schemas["Team"] = {
    "properties": {
        "type": {
            "enum": ["foaf:Agent"]
        },
        "name": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "creator": {
            "type": "string"
        }
    },
    "required": ["name"],
    "_primary_keys": [["name"]],
    "name": "Team",
    "source": "Agent",
    "_display": ["name", "description", "source"]
}


vv_builtin_schemas['VedavaapiSite'] = {
    "properties": {
        "type": {
            "enum": ["foaf:Agent"]
        },
        "name": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "logo": {
            "type": "object",
            "allowedClasses": ["StillImageRepresentation"]
        },
        "default_team": {
            "type": "string",
            "allowedClasses": ["Team"]
        }
    },
    "required": ["name"],
    "_primary_keys": [["type"]],
    "name": "VedavaapiSite",
    "source": "Resource",
    "_display": ["name", "description"]
}


vv_builtin_schemas['SnapshotAnnotation'] = {
    "properties": {
        "snapshot": {
            "type": "object",
            "allowedClasses": ["JsonObject"]
        },
        "notes": {
            "type": ["object", "string"]
        }
    },
    "required": ["snapshot", "target"],
    "source": "Annotation",
    "_display": ["snapshot", "notes", "target"],
    "name": "SnapshotAnnotation"
}


vv_builtin_schemas['ChoiceAnnotation'] = {
    "properties": {
        "membersClass": {
            "type": "string"
        },
        "choices": {
            "type": "object",
            "additionalProperties": {
                "type": "object",
                "properties": {
                    "up_voters": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            #  "allowedClasses": ["User"]
                        },
                    },
                    "down_voters": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            #  "allowedClasses": ["User"]
                        },
                    }
                },
                "required": ["up_voters", "down_voters"]
            }
        }
    },
    "required": ["membersClass", "choices"],
    "source": "Annotation",
    "_display": ["membersClass", "choices"],
    "name": "ChoiceAnnotation"
}

VVObjectKey = {
    "type": "object",
    "properties": {
        "className": {"type": "string"},
        "id": {"type": "string"},
    },
    "required": ["id"],
}


vv_builtin_schemas['Task'] = {
    "name":"Task",
    "source": "VVObject",
    "properties": {
        "name": {
            "type": "string",
        },
        "label": {
            "type": "object",
            "allowedClasses": ["Text"],
        },
        "service": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "id": {"type": "string"}
            },
            "required": ["name"],
        },
        "agent": {
            "type": "object",
            "properties": {
                "user": {
                    "type": "object",
                    "properties": {
                        "id": {"type": "string"},
                        "name": {"type": "string"},
                    },
                    "required": ["id"],
                },
                "teams": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "id": {"type": "string"},
                        },
                        "required": ["id"],
                    },
                },
            },
            "required": ["user"],
        },
        "actions": {
            "type": "array",
            "items": {
                "type": "string",
                "enum": ACTIONS,
            },
        },
        "status": {
            "type": "object",
            "properties": {
                "id": {"type": "string"},
                "url": {"type": "string", "format": "url"},
                "state": {
                    "type": "string",
                    "enum": [
                        "PENDING", "PROGRESS", "PARTIAL",
                        "SUCCESS", "FAILURE"
                    ],
                },
            },
            "required": ["state", "url"],
        },
        "on": {
            "type": "object",
            "properties": {
                "target": VVObjectKey,
                "path": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "meta_task_id": {"type": "string"},
                            "target": VVObjectKey,
                        }
                    },
                }
            },
            "required": ["target"]  # ?
        },
        "work": {
            "type": "object",
            "properties": {
                "work_type": {"type": "string"},
                "work_ext": {
                    "type": "array",
                    "items": {"type": "string"},
                },
                "quantum": {"type": "string"},
                "quantity": {"type": "number"},  # TODO succeeded, failed?
                "succeeded": {"type": "number"},
                "failed": {"type": "number"},
            },
            "required": ["work_type"],
        },
        "resolved": {"type": "boolean"},
        "chronology": {
            "type": "object",
            "properties": {
                "started_at": {"type": "string", "format": "date-time"},
                "updated_at": {"type": "string", "format": "date-time"},
                "resolved_at": {"type": "string", "format": "date-time"},
            },
            "required": ["started_at"],
        },
    },
    "required": [
        "resolved", "chronology", "status",
        "on", "work", "agent", "actions", "service", "name"
    ],
}

vv_builtin_schemas['FileStore'] = {
    "name":"FileStore",
    "source": "VVObject",
    "properties": {
        "name": {
            "type": "string",
        },
        "label": {
            "type": "object",
            "allowedClasses": ["Text"],
        },
        "type": {
            "enum": ["read-only", "read-write"],
            "default": "read-only",
        },
    }
}

vv_builtin_schemas['S3Store'] = {
    "name":"S3Store",
    "source": "FileStore",
    "properties": {
        "creds" : {
            "type" : "object",
            "properties" : {
                "key" : {
                    "type" : "string"
                },
                "secret" : {
                    "type" : "string"
                }
            }
        },
        "access_mode" : {
        },
        "url_template": {
            "type" : "string"
        },
        "endpoint_url": {
            "type": "string",
        }
    },
    "required": [
        "creds", "endpoint_url"
    ],
}

vv_builtin_schemas['LocalStore'] = {
    "name": "LocalStore",
    "source": "FileStore",
    "properties": {
        "root_path": {
            "type" : "string"
        }
    },
    "required": [
        "root_path"
    ]
}
