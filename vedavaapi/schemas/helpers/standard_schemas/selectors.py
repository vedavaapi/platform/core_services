schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "CssSelector",
        "properties": {
            "type": {
                "enum": [
                    "oa:CssSelector"
                ]
            },
            "value": {
                "type": "string"
            }
        },
        "required": [
            "value"
        ],
        "_display": [
            "value"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "FragmentSelector",
        "title": "Fragment Selector",
        "properties": {
            "type": {
                "enum": [
                    "oa:FragmentSelector"
                ]
            },
            "conformsTo": {
                "title": "confirms to",
                "type": "string"
            },
            "value": {
                "type": "string"
            },
            "rectangle": {
                "properties": {
                    "x": {
                        "type": "integer"
                    },
                    "y": {
                        "type": "integer"
                    },
                    "w": {
                        "type": "integer"
                    },
                    "h": {
                        "type": "integer"
                    }
                },
                "required": [
                    "x",
                    "y",
                    "w",
                    "h"
                ],
                "_display": [
                    "x",
                    "y",
                    "w",
                    "h"
                ]
            }
        },
        "required": [
            "value"
        ],
        "_display": [
            "conformsTo",
            "value"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "IndexSelector",
        "title": "SVG Selector",
        "properties": {
            "type": {
                "enum": [
                    "oa:Selector"
                ]
            },
            "index": {
                "type": [
                    "string",
                    "number"
                ]
            },
            "index_label": {
                "type": "string"
            }
        },
        "required": [
            "index"
        ],
        "_display": [
            "index"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "MembersSelector",
        "properties": {
            "field_filter": {
                "type": "object"
            },
            "index_filters": {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "index_field": {
                            "type": "string"
                        },
                        "indices": {
                            "type": "array",
                            "items": {
                                "type": [
                                    "number",
                                    "string"
                                ]
                            }
                        },
                        "range": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "from": {
                                        "type": "number"
                                    },
                                    "to": {
                                        "type": [
                                            "number",
                                            "string"
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "resource_ids": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        },
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "QualitativeSelector",
        "properties": {
            "type": {
                "enum": [
                    "oa:Selector"
                ]
            }
        },
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "RangeSelector",
        "properties": {
            "type": {
                "enum": [
                    "oa:RangeSelector"
                ]
            },
            "startSelector": {
                "type": "object",
                "allowedClasses": [
                    "Selector"
                ]
            },
            "endSelector": {
                "type": "object",
                "allowedClasses": [
                    "Selector"
                ]
            }
        },
        "required": [
            "startSelector",
            "endSelector"
        ],
        "_display": [
            "startSelector",
            "endSelector"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "SvgFragmentSelectorChoice",
        "title": "SVG Fragment Selector Choice",
        "properties": {
            "default": {
                "title": "Fragment Selector",
                "allowedClasses": ["FragmentSelector"]
            },
            "item": {
                "title": "SVG Selector",
                "allowedClasses": ["SvgSelector"]
            }
        },
        "required": [
            "default"
        ],
        "source": "SelectorChoice"
    },

    {
        "jsonClass": "VVSchema",
        "name": "SvgSelector",
        "title": "SVG Selector",
        "properties": {
            "type": {
                "enum": [
                    "oa:SvgSelector"
                ]
            },
            "value": {
                "type": "string"
            }
        },
        "required": [
            "value"
        ],
        "_display": [
            "value"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "TextPositionSelector",
        "properties": {
            "type": {
                "enum": [
                    "oa:TextPositionSelector"
                ]
            },
            "start": {
                "type": "integer",
                "minimum": 0
            },
            "end": {
                "type": "integer",
                "minimum": 0
            },
            "cached": {
                "type": "string"
            },
            "phrase": {
                "type": "string"
            }
        },
        "required": ["start", "end"],
        "_display": [
            "start",
            "end",
            "phrase"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "TextQuoteSelector",
        "properties": {
            "type": {
                "enum": [
                    "oa:TextQuoteSelector"
                ]
            },
            "exact": {
                "type": "string"
            },
            "prefix": {
                "type": "string"
            },
            "suffix": {
                "type": "string"
            }
        },
        "required": [
            "exact",
        ],
        "_display": [
            "exact",
            "prefix",
            "suffix"
        ],
        "source": "Selector"
    },

    {
        "jsonClass": "VVSchema",
        "name": "XpathSelector",
        "properties": {
            "type": {
                "enum": [
                    "oa:XpathSelector"
                ]
            },
            "value": {
                "type": "string"
            }
        },
        "required": [
            "value"
        ],
        "_display": [
            "value"
        ],
        "source": "Selector"
    }

]
