from . import annotations, books, logidocs


def get_profiles():
    all_modules = [annotations, books, logidocs]
    profiles = {}

    for module in all_modules:
        profiles.update(module.profiles)

    return profiles
