import flask_restx
from flask import g, request
from jsonschema import ValidationError

from vedavaapi.common.helpers.api_helper import error_response, jsonify_argument
from vedavaapi.common.helpers.args_parse_helper import parse_json_args
from vedavaapi.objectdb.helpers import ObjModelException, projection_helper

from . import api
from ...oauth_server_helpers import clients_helper
from ...agents_helpers import teams_helper
from .users_ns import _validate_projection

clients_ns = api.namespace('clients', path='/oauth/clients')


def marshal_to_google_structure(client_json):
    authorization_uri = request.url_root + 'accounts/oauth/v1/authorize'
    token_uri = request.url_root + 'accounts/oauth/v1/token'
    installed = {
        "client_id": client_json['client_id'],
        "client_secret": client_json.get('client_secret'),
        "redirect_uris": client_json.get('redirect_uris', []),
        "auth_uri": authorization_uri,
        "token_uri": token_uri
    }
    return {"installed": installed}


@clients_ns.route('')
class Clients(flask_restx.Resource):

    get_parser = clients_ns.parser()
    get_parser.add_argument('projection', type=str, location='args')
    get_parser.add_argument(
        'marshal_to_google_structure', type=str, location='args', default='false', choices=['true', 'false']
    )

    post_parser = clients_ns.parser()
    post_parser.add_argument('name', type=str, location='form', required=True)
    post_parser.add_argument(
        'grant_types', type=str, location='form', required=True,
        help='array of any of ("authorization_code", "refresh_token", "implicit", "client_credentials", "password")'
    )
    post_parser.add_argument('redirect_uris', type=str, location='form', required=False, help='array of redirect uris')
    post_parser.add_argument('client_type', type=str, location='form', required=True, choices=['public', 'private'])
    post_parser.add_argument('return_projection', type=str, location='form', required=False, default='{"permissions": 0}')
    post_parser.add_argument(
        'marshal_to_google_structure', type=str, location='form', default='false', choices=['true', 'false']
    )

    post_payload_json_parse_directives = {
        "redirect_uris":{"allowed_types": (list, ), "allow_none": True},
        "return_projection": {"allowed_types": (dict, ), "default": {"permissions": 0}, "custom_validator": _validate_projection},
        "marshal_to_google_structure": {"allowed_types": (bool, ), "default": False},
        "grant_types": {"allowed_types": (list, )}
    }

    @clients_ns.expect(get_parser, validate=True)
    def get(self):
        if g.current_user_id is None:
            return error_response(message='not authorized', code=401)

        args = self.get_parser.parse_args()
        projection = jsonify_argument(args.get('projection', None), key='projection')
        try:
            projection_helper.validate_projection(projection)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        clients_selector_doc = {
            "jsonClass": "OAuth2Client",
            "user_id": g.current_user_id
        }
        client_jsons = (g.oauth_colln.find(clients_selector_doc, projection=projection))
        if not jsonify_argument(args['marshal_to_google_structure']):
            return client_jsons
        return [marshal_to_google_structure(cj) for cj in client_jsons]

    @clients_ns.expect(post_parser, validate=True)
    def post(self):
        if g.current_user_id is None:
            return error_response(message='not authorized', code=401)

        current_user_team_ids = [
            team['_id'] for team in teams_helper.get_user_teams(
                g.users_colln, g.current_user_id, teams_projection={"_id": 1})
        ]

        args = parse_json_args(self.post_parser.parse_args(), self.post_payload_json_parse_directives)
        client_json = {
            "jsonClass": "OAuth2Client",
            "name": args['name'],
            "grant_types": args['grant_types'],
            "redirect_uris": args['redirect_uris']
        }
        for k in list(client_json.keys()):
            if not client_json[k]:
                client_json.pop(k)

        client_type = args['client_type']

        try:
            new_client_id = clients_helper.create_new_client(
                g.oauth_colln, g.acl_svc, client_json, client_type, g.current_user_id, current_user_team_ids, g.schema_validator, initial_agents=None)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except ValidationError as e:
            return error_response(
                message='invalid schema for client_json', code=403, details={"error": getattr(e, 'message', str(e))})

        new_client_json = g.oauth_colln.find_one({"_id": new_client_id})
        if not args['marshal_to_google_structure']:
            projection_helper.project_doc(new_client_json, args['return_projection'], in_place=True)
            return new_client_json

        return marshal_to_google_structure(new_client_json)


@clients_ns.route('/<client_id>')
class ClientResource(flask_restx.Resource):

    get_parser = clients_ns.parser()
    get_parser.add_argument('id_type', type=str, location='args', choices=['_id', 'client_id'], default='client_id')
    get_parser.add_argument('projection', type=str, location='args')
    get_parser.add_argument(
        'marshal_to_google_structure', type=str, location='args', default='false', choices=['true', 'false']
    )

    get_payload_json_parse_directives = {
        "projection": {"allowed_types": (dict, ), "allow_none": True, "custom_validator": _validate_projection},
        "marshal_to_google_structure": {"allowed_types": (bool,), "default": False},
    }

    @clients_ns.expect(get_parser, validate=True)
    def get(self, client_id):
        if g.current_user_id is None:
            return error_response(message='not authorized', code=401)

        current_user_team_ids = [
            team['_id'] for team in teams_helper.get_user_teams(
                g.users_colln, g.current_user_id, teams_projection={"_id": 1})
        ]

        args = parse_json_args(self.get_parser.parse_args(), self.get_payload_json_parse_directives)
        client_selector_doc = clients_helper.get_client_selector_doc(_id=client_id) if args['id_type'] == '_id' else clients_helper.get_client_selector_doc(client_id=client_id)
        client_json = g.oauth_colln.find_one(client_selector_doc)

        if not client_json:
            return error_response(message='client not found')
        if client_json['creator'] != g.current_user_id:
            return error_response(message='permission denied', code=403)

        '''
        if not PermissionResolver.resolve_permission(client_json, PermissionResolver.READ, g.current_user_id, current_user_team_ids, g.oauth_colln):
            return error_response(message='permission denied', code=403)
        '''

        if not args['marshal_to_google_structure']:
            projection_helper.project_doc(client_json, args['projection'], in_place=True)
            return client_json

        return marshal_to_google_structure(client_json)
