import json
import random
import string
import sys
import time

import bcrypt
from flask import g, current_app
import flask_restx
from vedavaapi.accounts import AuthorizationServer
from vedavaapi.acls.permissions_helper import PermissionResolver

from vedavaapi.objectdb.helpers import ObjModelException, projection_helper, objstore_helper

from vedavaapi.common.helpers.api_helper import error_response, jsonify_argument, check_argument_type, abort_with_error_response
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from . import api
from .users_ns import _validate_projection
from .. import sign_out_user
from ...agents_helpers import users_helper, teams_helper

me_ns = api.namespace('me', path='/me', description='personalization namespace')


def resolve_user_id():
    if current_token.user_id:
        return current_token.user_id
    elif g.current_user_id:
        return g.current_user_id
    else:
        message = "not authorized on behalf of any user" if current_token.client_id else "not authorized"
        error = error_response(message=message, code=401)
        abort_with_error_response(error)


# noinspection PyMethodMayBeStatic
@me_ns.route('')
class Me(flask_restx.Resource):

    get_parser = me_ns.parser()
    get_parser.add_argument('projection', type=str, location='args')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    post_parser = me_ns.parser()
    post_parser.add_argument('update_doc', type=str, location='form', required=True)
    post_parser.add_argument('return_projection', type=str, location='form')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    @me_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):
        user_id = resolve_user_id()
        args = self.get_parser.parse_args()

        projection = jsonify_argument(args.get('projection', None), key='projection')
        check_argument_type(projection, (dict,), key='projection', allow_none=True)
        _validate_projection(projection)
        projection = projection_helper.modified_projection(projection, mandatory_attrs=["_id", "jsonClass"])

        current_user_json = g.users_colln.find_one(
            users_helper.get_user_selector_doc(_id=user_id), projection=projection)
        if current_user_json is None:
            sign_out_user(g.current_org_name)
            return error_response(message='not authorized', code=401)
        return current_user_json, 200

    @me_ns.expect(post_parser, validate=True)
    @require_oauth(token_required=False)
    def post(self):
        user_id = resolve_user_id()
        team_ids = [
            team['_id'] for team in teams_helper.get_user_teams(
                g.users_colln, user_id, teams_projection={"_id": 1})
        ]

        args = self.post_parser.parse_args()
        update_doc = jsonify_argument(args['update_doc'], key='update_doc')
        check_argument_type(update_doc, (dict,), key='update_doc')
        if '_id' not in update_doc:
            update_doc['_id'] = user_id
        if 'jsonClass' not in update_doc:
            update_doc['jsonClass'] = 'User'

        if update_doc['_id'] != user_id:
            return error_response(message='invalid _id', code=403)
        if update_doc['jsonClass'] != 'User':
            return error_response(message='invalid jsonClass', code=403)

        if 'password' in update_doc and update_doc['password']:
            update_doc['password'] = bcrypt.hashpw(
                update_doc['password'].encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
        else:
            update_doc.pop('password', None)

        return_projection = jsonify_argument(args.get('return_projection', None), key='return_projection')
        check_argument_type(return_projection, (dict,), key='return_projection', allow_none=True)
        _validate_projection(return_projection)
        return_projection = projection_helper.modified_projection(
            return_projection, mandatory_attrs=['_id', 'jsonClass'])

        try:
            updated_user_id = objstore_helper.update_resource(
                g.users_colln, g.acl_svc, update_doc, user_id, team_ids, g.schema_validator,
                not_allowed_attributes=('externalAuthentications', 'password'))
            if updated_user_id is None:
                raise ObjModelException('user not exist', 404)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except ValueError as e:
            return error_response(message='schema validation error', code=403, details={"error": str(e)})

        user_json = g.users_colln.get(user_id, projection=return_projection)
        return user_json


@me_ns.route('/teams')
class UserTeams(flask_restx.Resource):

    get_parser = me_ns.parser()
    get_parser.add_argument(
        'teams_projection', type=str, location='args', default=None, help='projection for each team'
    )
    get_parser.add_argument('return_count', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    @me_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):
        user_id = resolve_user_id()

        args = self.get_parser.parse_args()

        teams_projection = jsonify_argument(args.get('teams_projection', None), key='teams_projection')
        check_argument_type(teams_projection, (dict, ), allow_none=True)
        _validate_projection(teams_projection)

        user_team_jsons = teams_helper.get_user_teams(g.users_colln, user_id, teams_projection=None)
        PermissionResolver.attach_resolved_permissions(
            g.users_colln, g.acl_svc, user_team_jsons,
            PermissionResolver.ACTIONS, user_id, [t['_id'] for t in user_team_jsons])

        for team_json in user_team_jsons:
            projection_helper.project_doc(team_json, teams_projection, in_place=True)

        should_return_count = json.loads(args.get('return_count', 'false'))
        if should_return_count:
            return {"count": len(user_team_jsons)}, 200

        return user_team_jsons, 200


@me_ns.route('/token')
class Token(flask_restx.Resource):
    get_parser = me_ns.parser()
    get_parser.add_argument(
        'scope', type=str, location='args', default='vedavaapi.root', help='space seperated scopes'
    )

    @me_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):
        user_id = resolve_user_id()
        args = self.get_parser.parse_args()

        user_team_jsons = teams_helper.get_user_teams(g.users_colln, user_id, teams_projection=None)
        team_ids = [t['_id'] for t in user_team_jsons]

        chars = string.ascii_letters + string.digits
        rand = random.SystemRandom()
        length = 30
        access_token = ''.join(rand.choice(chars) for _ in range(length))

        token_obj = {
            "jsonClass": "OAuth2Token",
            "client_id": None,
            "token_type": "Bearer",
            "access_token": access_token,
            "scope": args['scope'],
            "revoked": False,
            "issued_at": time.time(),
            "expires_in": g.authlib_config['OAUTH2_TOKEN_EXPIRES_IN']['client_credentials'],
            "user_id": user_id
        }

        g.oauth_colln.insert_one(token_obj)
        return {
            "access_token": access_token,
            "expires_in": token_obj['expires_in'],
            "token_type": "Bearer"
        }
