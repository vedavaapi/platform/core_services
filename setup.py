from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ''

setup(
    name='core_services',
    version='1.0.0',
    packages=['vedavaapi', 'vedavaapi.common', 'vedavaapi.acls', 'vedavaapi.accounts', 'vedavaapi.gservices', 'vedavaapi.credentials', 'vedavaapi.schemas'],
    url='https://github.com/vedavaapi',
    author='vedavaapi',
    description='core services of vedavaapi platform',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['pymongo', 'six', 'magic', 'jsonschema', 'furl'],
    classifiers=(
            "Programming Language :: Python :: 3.5",
            "Operating System :: OS Independent",
    )
)