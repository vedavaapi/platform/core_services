from vedavaapi.acls.permissions_helper import PermissionResolver
from vedavaapi.objectdb.helpers import projection_helper, ObjModelException, objstore_helper


def get_team_selector_doc(_id=None, team_name=None):
    if _id is not None:
        return {"jsonClass": "Team", "_id": _id}
    elif team_name is not None:
        return {"jsonClass": "Team", "name": team_name}
    else:
        return None


def get_team_id(teams_colln, team_name):
    team_json = teams_colln.find_one(get_team_selector_doc(team_name=team_name), projection={"_id": 1})
    return team_json['_id'] if team_json else None


def get_team_hierarchy(teams_colln, team_json, teams_projection=None):
    teams_projection = projection_helper.modified_projection(
        teams_projection, mandatory_attrs=["_id", "source"]
    )

    team_hierarchy = []
    parent_team_id = team_json.get('source', None)
    if not parent_team_id:
        return team_hierarchy

    parent_team_json = teams_colln.find_one(
        get_team_selector_doc(_id=parent_team_id), projection=teams_projection)
    if parent_team_json is None:
        return team_hierarchy

    team_hierarchy.append(parent_team_json)

    parent_team_hierarchy = get_team_hierarchy(
        teams_colln, parent_team_json, teams_projection=teams_projection)
    team_hierarchy.extend(parent_team_hierarchy)

    return team_hierarchy


def get_team_members(teams_colln, team_id, only_explicit_members=False):
    #  TODO arch
    team_json = teams_colln.find_one(
        get_team_selector_doc(_id=team_id), projection={"_id": 1, "source": 1, "members": 1})
    if not team_json:
        return None

    team_explicit_members = team_json.get('members', [])
    if only_explicit_members:
        return team_explicit_members

    resolved_members_set = set(team_explicit_members[:])
    child_team_ids = [
        team_json['_id'] for
        team_json in teams_colln.find(
            {"jsonClass": 'Team', "source": team_id}, projection={"_id": 1}
        )
    ]
    for child_team_id in child_team_ids:
        child_team_members = get_team_members(teams_colln, child_team_id, only_explicit_members=False)
        resolved_members_set.update(child_team_members)

    return list(resolved_members_set)


def get_user_teams(teams_colln, user_id, teams_projection=None):
    teams_projection = projection_helper.modified_projection(
        teams_projection, mandatory_attrs=["_id", "source"]
    )

    team_id_jsons_map = {}
    explicit_team_jsons = teams_colln.find(
        {"jsonClass": 'Team', "members": user_id}, projection=teams_projection
    )
    team_id_jsons_map.update(dict((team_json['_id'], team_json) for team_json in explicit_team_jsons))

    for team_json in explicit_team_jsons:
        team_hierarchy = get_team_hierarchy(teams_colln, team_json, teams_projection=teams_projection)
        team_id_jsons_map.update(dict((team_json['_id'], team_json) for team_json in team_hierarchy))

    return list(team_id_jsons_map.values())


'''
functions for creating team
'''


def create_new_team(
        teams_colln, acl_svc, team_json, user_id, user_team_ids, schema_validator,
        initial_agents=None, validate_schema=True, validate_permissions=True):

    for k in list(team_json.keys()):
        if team_json[k] is None:
            team_json.pop(k)

    for k in ('_id', 'members'):
        if k in team_json:
            raise ObjModelException('you cannot set "{}" attribute.'.format(k), 403)

    if team_json.get('jsonClass', None) != 'Team':
        raise ObjModelException('invalid jsonClass', 403)

    if 'source' not in team_json and initial_agents:
        team_json['source'] = initial_agents.all_users_team_id

    team_json['members'] = []
    if user_id:
        team_json['members'].append(user_id)

    new_team_id = objstore_helper.create_or_update(
        teams_colln, acl_svc, team_json, user_id, user_team_ids, schema_validator=schema_validator,
        initial_agents=initial_agents, validate_schema=validate_schema, validate_permissions=validate_permissions)
    acl_svc.add_to_agent_ids(
        [new_team_id], [PermissionResolver.READ], PermissionResolver.GRANT, team_ids=[new_team_id])

    return new_team_id


def add_users_to_team(
        users_colln, acl_svc, team_selector_doc, user_identifiers, current_user_id, current_team_ids,
        user_identifiers_type='_id', validate_permissions=True):

    if user_identifiers_type not in ('email', '_id'):
        return ObjModelException('invalid user_identifiers_type', 400)

    team = users_colln.find_one(
        team_selector_doc, projection={"jsonClass": 1, "_id": 1, "source": 1})
    if team is None:
        raise ObjModelException('team not found', 404)

    if validate_permissions and not PermissionResolver.resolve_permission(
            team, 'updateContent', current_user_id, current_team_ids, users_colln, acl_svc):
        raise ObjModelException('permission denied', 403)

    user_ids = []
    for user_identifier in user_identifiers:
        from . import users_helper
        user_primary_key_doc = {user_identifiers_type: user_identifier}
        user_json = users_colln.find_one(
            users_helper.get_user_selector_doc(**user_primary_key_doc), projection={"_id": 1})
        if user_json is None:
            raise ObjModelException('user "{}" not found'.format(user_identifier), 403)
        user_ids.append(user_json['_id'])

    update_doc = {"$addToSet": {"members": {"$each": user_ids}}}
    response = users_colln.update_one(team_selector_doc, update_doc)

    return response.modified_count


def remove_users_from_team(
        users_colln, acl_svc, team_selector_doc, user_identifiers, current_user_id, current_team_ids,
        user_identifiers_type='_id', validate_permissions=True):

    team = users_colln.find_one(
        team_selector_doc, projection={"jsonClass": 1, "_id": 1, "source": 1, "permissions": 1})
    if team is None:
        raise ObjModelException('team not found', 404)

    if validate_permissions and not PermissionResolver.resolve_permission(
            team, 'updateContent', current_user_id, current_team_ids, users_colln, acl_svc):
        raise ObjModelException('permission denied', 403)

    if user_identifiers_type == '_id':
        user_ids = user_identifiers
    else:
        user_ids = []
        for user_identifier in user_identifiers:
            from . import users_helper
            user_primary_key_doc = {user_identifiers_type: user_identifier}
            user_json = users_colln.find_one(
                users_helper.get_user_selector_doc(**user_primary_key_doc), projection={"_id": 1})
            if user_json is None:
                continue
            user_ids.append(user_json['_id'])

    update_doc = {"$pull": {"members": {"$in": user_ids}}}
    response = users_colln.update_one(team_selector_doc, update_doc)

    return response.modified_count
