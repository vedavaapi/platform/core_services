import functools

from flask import session, g
from vedavaapi.common.helpers.api_helper import get_current_org
from vedavaapi.objectdb.mydb import MyDbCollection

from . import myservice


def _get_current_user_id():
    current_org_name = get_current_org()
    authentications = session.get('authentications', {})
    if current_org_name not in authentications:
        return None
    return authentications[current_org_name]['user_id']


def _get_acls_colln():
    current_org_name = get_current_org()
    acls_colln = myservice().colln(current_org_name)  # type: MyDbCollection
    return acls_colln


def _get_objstore_colln():
    current_org_name = get_current_org()
    objstore_colln = myservice().registry.lookup('objstore').colln(current_org_name)
    return objstore_colln


def _get_acl_svc():
    current_org_name = get_current_org()
    acls_service = myservice()
    return acls_service.get_acl_svc(current_org_name)


def push_environ_to_g():
    g.acls_colln = _get_acls_colln()
    g.objstore_colln = _get_objstore_colln()
    g.current_org_name = get_current_org()
    g.current_user_id = _get_current_user_id()
    g.acl_svc = _get_acl_svc()


def set_environ(func):
    @functools.wraps(func)
    def _with_environ(*args, **kwargs):
        push_environ_to_g()
        return func(*args, **kwargs)
    return _with_environ
