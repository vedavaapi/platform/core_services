schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "Sequence",
        "properties": {
            "source": {
                "type": "string"
            },
            "selector": {
                "type": "object"
            },
            "members": {
                "type": "object",
                "allowedClasses": ["MembersSelector"]
            },
            "memberClass": {
                "type": "string"
            }
        },
        "required": ["selector", "source"],
        "_display": ["selector", "source", "memberClass", "jsonClassLabel"],
        "_primary_keys": [["source", "selector"]],
        "source": "Resource"
    }

]
