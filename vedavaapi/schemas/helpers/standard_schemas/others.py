schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "Service",
        "properties": {
            "name": {
                "type": "string"
            },
            "url": {
                "type": "string",
                "format": "uri"
            },
            "handledTypes": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "apis": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        },
        "required": [
            "jsonClass",
            "name",
            "url"
        ],
        "_display": [
            "name",
            "url",
            "apis"
        ],
        "_primary_keys": [["name"]],
        "source": "VVObject"
    }

]
