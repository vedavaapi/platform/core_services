import sys

import bcrypt

from vedavaapi.objectdb.helpers import ObjModelException, projection_helper, objstore_helper
from vedavaapi.acls.permissions_helper import PermissionResolver


def get_user_selector_doc(_id=None, email=None, external_provider=None, external_uid=None):
    if _id is not None:
        selector_doc = {"jsonClass": "User", "_id": _id}

    elif email is not None:
        selector_doc = {"jsonClass": "User", "email": email}

    elif external_provider is not None and external_uid is not None:
        selector_doc = {
            "jsonClass": "User",
            "externalAuthentications": {"$elemMatch": {"provider": external_provider, "uid": external_uid}}
        }
    else:
        selector_doc = None

    return selector_doc


'''
def get_user(users_colln, user_selector_doc, projection=None):
    projection = projection_helper.modified_projection(projection, mandatory_attrs=["jsonClass"])
    user_json = users_colln.find_one(user_selector_doc, projection=projection)
    return JsonObject.make_from_dict(user_json)
'''


def project_user_json(user_json, projection=None, in_place=False):
    user_exposed_projection = projection_helper.modified_projection(
        user_json.get('exposed_projection', None), mandatory_attrs=['_id', 'jsonClass']) or {}
    user_exposed_projection = projection_helper.get_restricted_projection(
        user_exposed_projection, {"password": 0}
    )
    projected_user_json = projection_helper.project_doc(
        user_json, projection_helper.get_restricted_projection(projection, user_exposed_projection), in_place=in_place)
    return projected_user_json


# noinspection PyProtectedMember
def get_user_id(users_colln, email):
    user_json = users_colln.find_one(get_user_selector_doc(email=email), projection={"_id": 1})
    return user_json['_id'] if user_json else None


'''
helpers over user object
purely JsonObject related operations. no db operations
'''


def is_provider_linked(user, provider_name):
    """

    :type user: User
    :param provider_name:
    :return:
    """
    if not hasattr(user, 'externalAuthentications'):
        return False
    for auth_info in user.__getattribute__('externalAuthentications'):
        if auth_info.provider == provider_name:
            return True
    return False


def get_provider_uid(user, provider_name):
    """

    :type user: User
    :param provider_name:
    :return:
    """
    if not hasattr(user, 'externalAuthentications'):
        return None
    for auth_info in user.__getattribute__('externalAuthentications'):
        if auth_info.provider == provider_name:
            return auth_info.uid
    return None


def check_password(user_json, password):
    return bcrypt.checkpw(password.encode('utf-8'), user_json['password'].encode('utf-8'))


'''
functions for modifying existing users in collection
purely basic db operations
no validity checks.
'''


def create_new_user(
        users_colln, acl_svc, user_json, schema_validator,
        initial_agents=None, initial_team_id=None, password_required=True,
        validate_schema=True, validate_permissions=True):

    for k in list(user_json.keys()):
        if user_json[k] is None:
            user_json.pop(k)

    for k in ('_id', 'externalAuthentications'):
        if k in user_json:
            raise ObjModelException('you cannot set "{}" attribute.', 403)

    essential_fields = ['email', 'jsonClass']
    if password_required:
        essential_fields.append('password')
    for k in essential_fields:
        if k not in user_json:
            raise ObjModelException('{} should be provided for creating new user'.format(k), 400)

    if user_json['jsonClass'] != 'User':
        raise ObjModelException('invalid jsonClass', 403)

    if 'password' in user_json:
        user_json['password'] = bcrypt.hashpw(
            user_json['password'].encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

    existing_user_json = users_colln.find_one(
        get_user_selector_doc(email=user_json['email']),
        projection={"_id": 1, "jsonClass": 1})
    if existing_user_json is not None:
        raise ObjModelException('user already exists', 403)

    user_json['externalAuthentications'] = {"jsonClass": "WrapperObject"}

    new_user_id = objstore_helper.create_resource(
        users_colln, acl_svc, user_json, None, None, schema_validator,
        initial_agents=initial_agents, validate_schema=validate_schema, validate_permissions=validate_permissions
    )
    acl_svc.add_to_agent_ids(
        [new_user_id], PermissionResolver.ACTIONS, PermissionResolver.GRANT, user_ids=[new_user_id])

    if initial_agents and hasattr(initial_agents, 'all_users_team_id'):
        acl_svc.add_to_agent_ids(
            [new_user_id], [PermissionResolver.READ],
            PermissionResolver.GRANT, team_ids=[initial_agents.all_users_team_id])

    from ..agents_helpers import teams_helper
    if initial_team_id is not None:
        teams_helper.add_users_to_team(
            users_colln, acl_svc,
            teams_helper.get_team_selector_doc(_id=initial_team_id), [new_user_id],
            None, None, validate_permissions=False)

    return new_user_id


def add_external_authentication(users_colln, user_selector_doc, auth_info):
    update_doc = {
        "$set": {
            "externalAuthentications.{}".format(auth_info['provider']):
                auth_info
        }
    }
    if ('picture' in auth_info):
        update_doc['$set']['picture'] = auth_info['picture']
    response = users_colln.update_one(user_selector_doc, update_doc)
    return response.modified_count


def remove_external_authentication(users_colln, user_selector_doc, provider_name):
    update_doc = {
        "$unset": dict(('externalAuthentications.{}'.format(p), '') for p in [provider_name])
    }
    response = users_colln.update_one(user_selector_doc, update_doc)
    return response.modified_count
