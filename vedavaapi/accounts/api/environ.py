import functools
import os

from flask import session, g
from vedavaapi.common.helpers.api_helper import get_current_org
from vedavaapi.objectdb.mydb import MyDbCollection

from . import myservice
from ..oauth_server_helpers.authorization_server import AuthorizationServer


def _get_current_user_id():
    current_org_name = get_current_org()
    authentications = session.get('authentications', {})
    if current_org_name not in authentications:
        return None
    return authentications[current_org_name]['user_id']


def _get_users_colln():
    current_org_name = get_current_org()
    _users_colln = myservice().get_users_colln(current_org_name)  # type: MyDbCollection
    return _users_colln


def _get_oauth_colln():
    current_org_name = get_current_org()
    _oauth_colln = myservice().get_oauth_colln(current_org_name)  # type: MyDbCollection
    return _oauth_colln


def _get_authlib_config():
    current_org_name = get_current_org()
    return myservice().get_org(current_org_name).authlib_config


def _get_authlib_authorization_server():
    current_org_name = get_current_org()
    _authorization_server = myservice().get_authlib_authorization_server(current_org_name)  # type: AuthorizationServer
    return _authorization_server


def _get_authorizer_config():
    return myservice().get_authorizer_config()


def _get_initial_agents():
    current_org_name = get_current_org()
    return myservice().get_initial_agents(current_org_name)


def _get_token_introspection_endpoint():
    current_org_name = get_current_org()

    url_root = g.original_url_root
    token_resolver_endpoint = os.path.join(
        url_root.lstrip('/'),
        current_org_name,
        'accounts/oauth/v1/introspect_token'
    )
    return token_resolver_endpoint


def _get_schema_validator():
    current_org_name = get_current_org()
    schema_service = myservice().registry.lookup('schemas')
    return schema_service.get_schema_validator(current_org_name, _get_users_colln())


def _get_acl_svc():
    current_org_name = get_current_org()
    acls_service = myservice().registry.lookup('acls')
    return acls_service.get_acl_svc(current_org_name)


def push_environ_to_g():
    g.users_colln = _get_users_colln()
    g.oauth_colln = _get_oauth_colln()
    g.authlib_config = _get_authlib_config()
    g.authorization_server = _get_authlib_authorization_server()
    g.authorizer_config = _get_authorizer_config()
    g.current_org_name = get_current_org()
    g.schema_validator = _get_schema_validator()
    g.current_user_id = _get_current_user_id()
    g.initial_agents = _get_initial_agents()
    g.all_users_team_json = g.users_colln.find_one({"_id": g.initial_agents.all_users_team_id})
    g.acl_svc = _get_acl_svc()
    g.token_introspection_endpoint = _get_token_introspection_endpoint()


def set_environ(func):
    @functools.wraps(func)
    def _with_environ(*args, **kwargs):
        push_environ_to_g()
        return func(*args, **kwargs)
    return _with_environ
