import flask_restx
from flask import Blueprint


api_blueprint_v1 = Blueprint('acls'+'_v1', __name__, static_folder='static', static_url_path='/static')


api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Vedavaapi Acls',
    description='Vedavaapi Acls management api',
    doc='/docs')

from . import rest
