def purge_nones(doc: dict):
    for k in list(doc.keys()):
        if doc[k] == None:
            doc.pop(k)
    return doc

def get_text(chars: str, language=None, script=None):
    txt = {
        "jsonClass": "Text",
        "chars": chars,
        "language": language,
        "script": script,
    }
    purge_nones(txt)
    return txt
