from authlib.integrations.flask_oauth2 import current_token
from vedavaapi.accounts.api import require_oauth
from vedavaapi.common.helpers.api_helper import get_current_org


@require_oauth()
def resolve_token_functionally():
    from vedavaapi.accounts import VedavaapiAccounts
    accounts_service = VedavaapiAccounts.instance
    current_org_name = get_current_org()
    oauth_colln = accounts_service.get_oauth_colln(current_org_name)
    users_colln = accounts_service.get_users_colln(current_org_name)

    token_json = current_token.json.copy()
    client_id = token_json['client_id']
    # print(token, client_id)

    if 'user_id' not in token_json or not token_json['user_id']:
        client_json = oauth_colln.find_one({"client_id": client_id}, projection={"user_id": 1, "creator": 1})
        token_json['user_id'] = client_json.get('user_id', client_json['creator'])

    from vedavaapi.accounts.agents_helpers import teams_helper
    team_ids = [
        team['_id'] for team in teams_helper.get_user_teams(
            users_colln, token_json['user_id'], teams_projection={"_id": 1}
        )
    ]
    token_json['team_ids'] = team_ids

    return token_json
