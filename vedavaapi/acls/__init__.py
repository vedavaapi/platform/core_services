import logging

from vedavaapi.objectdb.mydb import MyDbCollection
from vedavaapi.common import VedavaapiService, OrgHandler

from .acl_service import AclSvc

logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)


class AclsOrgHandler(OrgHandler):
    def __init__(self, service, org_name):
        super(AclsOrgHandler, self).__init__(service, org_name)

        self.acls_db_config = self.dbs_config['acls_db']
        self.acls_db = self.store.db(self.acls_db_config['name'])
        self.acls_colln = self.acls_db.get_collection(
            self.acls_db_config['collections']['objstore']
        )


class VedavaapiAcls(VedavaapiService):

    instance = None  # type: VedavaapiAcls

    dependency_services = []
    org_handler_class = AclsOrgHandler

    title = "Vedavaapi Acls"
    description = "acls api"

    def __init__(self, registry, name, conf):
        super(VedavaapiAcls, self).__init__(registry, name, conf)

    def init_service(self):
        for org_name in self.registry.org_names:
            self.get_org(org_name)  # initializes all orgs.

    def colln(self, org_name):
        return self.get_org(org_name).acls_colln  # type: MyDbCollection

    def get_acl_svc(self, org_name):
        colln = self.colln(org_name)
        if not colln:
            return None
        return AclSvc(colln, org_name=org_name)
