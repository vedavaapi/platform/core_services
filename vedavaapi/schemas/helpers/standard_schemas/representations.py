schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "DataRepresentation",
        "properties": {
            "data": {
                "type": "string",
                "pattern": "^_OOLD:.*$",
                "description": "should be link to ool data"
            },
            "implements": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        },
        "_display": [
            "data",
            "implements"
        ],
        "source": "JsonObject"
    },

    {
        "jsonClass": "VVSchema",
        "name": "DataRepresentations",
        "properties": {
            "default": {
                "enum": [
                    "stillImage",
                    "movingImage",
                    "sound",
                    "dataSet",
                    "interactiveResource"
                ]
            }
        },
        "additionalProperties": {
            "type": "array",
            "items": {
                "type": "object",
                "allowedClasses": [
                    "DataRepresentation"
                ]
            }
        },
        "_display": [
            "stillImage",
            "movingImage",
            "sound",
            "dataSet",
            "interactiveResource"
        ],
        "required": [
            "default"
        ],
        "source": "JsonObject"
    },

    {
        "jsonClass": "VVSchema",
        "name": "StillImageRepresentation",
        "properties": {
            "material": {
                "enum": [
                    "paper",
                    "taaLapatra",
                    "cloth",
                    "stone",
                    "metal"
                ]
            },
            "materialQualifiers": {
                "type": "object"
            },
            "objectType": {
                "type": "string"
            },
            "script": {
                "type": "string"
            }
        },
        "source": "DataRepresentation"
    }

]
