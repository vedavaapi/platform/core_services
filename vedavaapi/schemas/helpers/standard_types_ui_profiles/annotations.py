from ._utils import get_text

profiles = {}

profiles['TextAnnotation'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Text annotation", language="en"),
                get_text("పాఠ్య ఉల్లేఖనము", "te"),
            ],
            "label_plural": [
                get_text("Text annotations", language="en"),
                get_text("పాఠ్యోల్లేఖనాలు", "te"),
            ]
        },
        "instance": {
            "txt_repr": "body",
        }
    },
    "search": {
        "simple": {
            "fields": ["body.chars"],
            "label": [
                get_text("content", "en"),
                get_text("పాఠ్యం", "te"),
            ],
        },
        "advanced": [
            {
                "field": "body.chars",
                "label": [
                    get_text("content", "en"),
                    get_text("పాఠ్యం", "te"),
                ],
                "type": "string",
            },
            {
                "field": "target",
                "type": "_id",
            }
        ]
    }
}
