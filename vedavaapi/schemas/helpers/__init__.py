from copy import deepcopy

from jsonschema import validators
from vv_schemas import VVSchemas as _VVSchemas


class VVSchemas(_VVSchemas):

    def __init__(self, colln):
        super(VVSchemas, self).__init__()
        self.colln = colln
        self._schemas_cache = {}
        self._cached_ids_to_defs_map = {}

    @classmethod
    def _compute_meta_schema(cls, partial_schema):
        # impure
        draft4_meta_schema = deepcopy(validators.Draft4Validator.META_SCHEMA)
        effective_props = partial_schema.pop('properties')
        effective_props.update(draft4_meta_schema.pop('properties'))

        partial_schema.update(draft4_meta_schema)
        partial_schema['properties'] = effective_props
        return partial_schema

    def _get(self, _id=None, json_class=None):
        if not _id and not json_class:
            return None

        cached_json_class_name = None
        if _id and _id in self._cached_ids_to_defs_map:
            cached_json_class_name = self._cached_ids_to_defs_map[_id]
        elif json_class and json_class in self._schemas_cache:
            cached_json_class_name = json_class
        if cached_json_class_name:
            return self._schemas_cache[cached_json_class_name]

        selector_doc = {"jsonClass": "VVSchema"}
        if _id is not None:
            selector_doc['_id'] = _id
        else:
            selector_doc['name'] = json_class

        vv_schema = self.colln.find_one(selector_doc)
        if not vv_schema:
            if json_class:
                self._schemas_cache[json_class] = vv_schema or None
            return None

        if vv_schema['name'] == 'VVSchema':
            vv_schema = self._compute_meta_schema(vv_schema)

        self._schemas_cache[vv_schema['name']] = vv_schema
        self._cached_ids_to_defs_map[vv_schema['_id']] = vv_schema['name']
        return vv_schema

    def get(self, json_class_name):
        return self._get(json_class=json_class_name)

    def _get_inheritance_branch(self, schema):
        branch = []
        branch.append(schema['name'])

        parent_id = schema.get('source', None)
        if parent_id:
            parent_obj = self._get(_id=parent_id)
            if parent_obj:
                parent_inheritance_branch = self._get_inheritance_branch(parent_obj)
                branch.extend(parent_inheritance_branch)

        return branch

    def get_ancestors(self, json_class):
        schema = self.get(json_class)
        if not schema:
            return []
        inheritance_branch = self._get_inheritance_branch(schema)
        return inheritance_branch[1:]
