schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "ImageRegion",
        "properties": {
            "selector": {
                "type": "object",
                "allowedClasses": [
                    "FragmentSelector",
                    "SvgSelector",
                    "SvgFragmentSelectorChoice"
                ]
            }
        },
        "required": [
            "selector",
            "source"
        ],
        "_display": [
            "selector",
            "source"
        ],
        "source": "Resource"
    },

    {
        "jsonClass": "VVSchema",
        "name": "Library",
        "properties": {
            "name": {
                "title": "library name",
                "type": "string"
            },
            "description": {
                "title": "library description",
                "type": "string"
            },
            "logo": {
                "type": "object",
                "allowedClasses": [
                    "StillImageRepresentation"
                ]
            }
        },
        "required": [
            "name",
        ],
        "_display": [
            "name",
            "description",
            "source",
            "logo"
        ],
        "_primary_keys": [
            [
                "name",
                "source"
            ]
        ],
        "source": "Resource",
        "_iiif_model": {
            "implements": [
                "collection"
            ],
            "data_fields": {
                "label": "name",
                "metadata": "metadata",
                "description": "description"
            },
            "object_links": {
                "referrer_field_filter_maps": {
                    "source": {
                        "jsonClass": "ScannedBook"
                    }
                }
            },
            "sub_collection_links": {
                "referrer_field_filter_maps": {
                    "source": {
                        "jsonClass": "Library"
                    }
                }
            }
        }
    },


    {
        "jsonClass": "VVSchema",
        "name": "ScannedBook",
        "properties": {
            "type": {
                "enum": [
                    "dcterms:BibliographicResource"
                ]
            },
            "title": {
                "type": "string"
            },
            "author": {
                "title": "authors",
                "type": "array",
                "items": {
                    "type": "string"
                },
                "minItems": 1
            },
            "description": {
                "title": "book description",
                "type": "string"
            },
            "cover": {
                "type": "object",
                "allowedClasses": [
                    "StillImageRepresentation"
                ]
            },
            "representations": {
                "type": "object",
                "allowedClasses": [
                    "DataRepresentations"
                ]
            }
        },
        "required": [
            "title",
        ],
        "_display": [
            "author",
            "title",
            "cover",
            "source",
            "description",
        ],
        "_primary_keys": [
            [
                "source",
                "title"
            ]
        ],
        "source": "Resource",
        "_iiif_model": {
            "implements": [
                "object",
                "sequence"
            ],
            "data_fields": {
                "label": "title",
                "metadata": "metadata"
            },
            "sequence_links": {
                "referrer_field_filter_maps": {
                    "target": {
                        "jsonClass": "SequenceAnnotation"
                    }
                }
            },
            "default_sequence_links": {
                "referrer_field_filter_maps": {
                    "target": {
                        "jsonClass": "SequenceAnnotation",
                        "canonical": "default_canvas_sequence"
                    }
                }
            },
            "fallback_on_self_for_default_sequence": True,
            "canvas_links": {
                "referrer_field_filter_maps": {
                    "source": {
                        "jsonClass": "ScannedPage"
                    }
                }
            }
        }
    },

    {
        "jsonClass": "VVSchema",
        "name": "ScannedPage",
        "title": "ScannedPage",
        "properties": {
            "type": {
                "enum": [
                    "dcterms:BibliographicResource"
                ]
            },
            "representations": {
                "type": "object",
                "allowedClasses": [
                    "DataRepresentations"
                ]
            }
        },
        "reuired": [
            "source"
        ],
        "_display": [
            "purpose",
            "source"
        ],
        "_primary_keys": [
            [
                "source",
                "selector"
            ]
        ],
        "source": "Resource",
        "_iiif_model": {
            "implements": [
                "canvas"
            ],
            "data_fields": {
                "label": "selector.index",
                "height": "representations.stillImage.0.height",
                "width": "representations.stillImage.0.width",
                "metadata": "metadata",
                "label_prefix": "jsonClassLabel"
            },
            "image_links": {
                "fields": [
                    "representations.stillImage.0.data"
                ]
            }
        }
    },

    {
        "jsonClass": "VVSchema",
        "name": "TextDocument",
        "properties": {
            "title": {
                "type": "string"
            },
            "author": {
                "type": "array",
                "items": {
                    "type": "string"
                },
                "minItems": 1
            },
            "description": {
                "title": "book description",
                "type": "string"
            }
        },
        "source": "Resource",
        "_display": ["source", "title", "author", "description"]
    },

    {
        "jsonClass": "VVSchema",
        "name": "TextSection",
        "source": "Resource",
        "properties": {
            "content": {
                "type": "string"
            },
            "components": {
                "type": "array",
                "items": {
                    "type": "object",
                    "allowedClasses": ["Text"]
                }
            }
        },
        "_display": ["source", "content"]
    }

]
