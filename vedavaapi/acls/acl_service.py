from .permissions_helper import PermissionResolver
from vedavaapi.objectdb.mydb import MyDbCollection
from vv_schemas import SchemaValidator


def myservice():
    # TODO should be removed
    from . import VedavaapiAcls
    return VedavaapiAcls.instance


class AclSvc(object):

    def __init__(self, colln, org_name=None):
        self.org_name = org_name
        self.colln = colln  # type: MyDbCollection

    def get(self, _id):
        return self.colln.find_one({
            "jsonClass": "Acl",
            "_id": _id
        })

    def find(self, selector_doc, projection=None):
        return self.colln.find(selector_doc, projection=projection)

    def update(self, _id, acl, upsert=False, validate_schema=False):
        if validate_schema and self.org_name:
            schema_validator = myservice().registry.lookup('schemas').get_schema_validator(self.org_name, self.colln)  # type: SchemaValidator  # TODO should be supplied passively
            schema_validator.validate(acl)

        resp = self.colln.update_one(
            {"_id": _id, "jsonClass": "Acl"}, {"$set": acl}, upsert=upsert)

        return resp

    def delete(self, _ids):
        selector_doc = {"_id": {"$in": _ids}}
        return self.colln.delete_many(selector_doc)

    def add_to_agent_ids(self, resource_ids, actions, control, user_ids=None, team_ids=None):
        if not team_ids and not user_ids:
            return None

        selector_doc = {"_id": {"$in": resource_ids}}
        update_doc = {"$addToSet": {}}
        for action in actions:
            if action not in PermissionResolver.ACTIONS:
                continue
            update_doc['$addToSet'].update({
                "{}.{}.users".format(action, control): {
                    "$each": user_ids or []
                },
                "{}.{}.teams".format(action, control): {
                    "$each": team_ids or []
                }
            })
        return self.colln.update_many(selector_doc, update_doc)

    def remove_from_agent_ids(self, resource_ids, actions, control, user_ids=None, team_ids=None):
        if not user_ids and not team_ids:
            return None

        selector_doc = {"_id": {"$in": resource_ids}}
        update_doc = {"$pull": {}}
        for action in actions:
            update_doc['$pull'].update({
                "{}.{}.users".format(action, control): {
                    "$in": user_ids or []
                },
                "{}.{}.teams".format(action, control): {
                    "$in": team_ids or []
                }
            })
        return self.colln.update_many(selector_doc, update_doc)
