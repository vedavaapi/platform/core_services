
class PermissionResolver(object):

    READ = 'read'
    UPDATE_CONTENT = "updateContent"
    UPDATE_LINKS = "updateLinks"
    UPDATE_PERMISSIONS = "updatePermissions"
    DELETE = "delete"
    CREATE_CHILDREN = "createChildren"
    CREATE_ANNOS = "createAnnos"

    ACTIONS = (READ, UPDATE_CONTENT, UPDATE_LINKS, UPDATE_PERMISSIONS, DELETE, CREATE_CHILDREN, CREATE_ANNOS)

    GRANT = 'grant'
    REVOKE = 'revoke'
    BLOCK = 'block'
    CONTROLS = (GRANT, REVOKE, BLOCK)

    @classmethod
    def has_nested_key(cls, doc, *nested_key_hierarchy):
        val_node = doc

        for k in nested_key_hierarchy:
            if not isinstance(val_node, dict):
                return False
            if k not in val_node:
                return False
            val_node = val_node[k]

        return True

    @classmethod
    def referred_object_ids(cls, doc):
        referred_object_ids_val = doc.get('source', doc.get('target', None))
        if not referred_object_ids_val:
            return None
        return referred_object_ids_val if isinstance(referred_object_ids_val, list) else [referred_object_ids_val]

    @classmethod
    def get_id_cards_for_action(cls, acl, action, control):
        id_cards = {}
        if not acl:
            return id_cards

        for agent_type in ('users', 'teams'):
            if cls.has_nested_key(acl, action, control, agent_type):
                id_cards[agent_type] = acl[action][control][agent_type]
        return id_cards

    @classmethod
    def _ensure_objects_in_graph(cls, colln, object_ids, cache_graph, projection=None):
        non_cached_ids = list(set(object_ids).difference(cache_graph.keys()))
        if len(non_cached_ids):
            objects = colln.find(
                {"_id": {"$in": non_cached_ids}},
                projection=projection
            )
            cache_graph.update(dict((obj['_id'], obj) for obj in objects))
        return non_cached_ids

    @classmethod
    def get_referred_resources_graphs(
            cls, resources_colln, doc, crawled_graph, in_place=False):

        referred_ids_cumulative_set = set()
        referred_resources_graph = crawled_graph if in_place else crawled_graph.copy()

        referred_ids = cls.referred_object_ids(doc)
        if not referred_ids or not resources_colln:
            return referred_resources_graph, referred_ids_cumulative_set

        now_crawled_ids = cls._ensure_objects_in_graph(
            resources_colln, referred_ids, referred_resources_graph, projection=None)  # NOTE projection
        referred_ids_cumulative_set.update(referred_ids)

        for referred_id in referred_ids:
            referred_resources_graph, nl_referred_ids_cumulative_set = cls.get_referred_resources_graphs(
                resources_colln, referred_resources_graph[referred_id], referred_resources_graph, in_place=True)
            referred_ids_cumulative_set.update(nl_referred_ids_cumulative_set)

        return referred_resources_graph, referred_ids_cumulative_set

    @classmethod
    def resolve_permission(
            cls, resource, action, user_id, team_ids, resources_colln, acl_svc,
            resource_graph_cache=None, acls_cache=None
    ):

        if action not in cls.ACTIONS:
            return False

        acl = acl_svc.get(resource['_id'])
        referred_resources_graph = resource_graph_cache if resource_graph_cache is not None else {}
        referred_resources_graph[resource['_id']] = resource
        referred_resources_graph, referred_ids_cumulative_set = cls.get_referred_resources_graphs(
            resources_colln, resource, referred_resources_graph, in_place=True)

        referred_acls_graph = acls_cache if acls_cache is not None else {}
        if acl:
            referred_acls_graph[acl['_id']] = acl
        cls._ensure_objects_in_graph(acl_svc, list(referred_ids_cumulative_set), referred_acls_graph)

        return cls.resolve_permission_on_res_with_referred_graph(
            resource['_id'], referred_resources_graph, referred_acls_graph, action, user_id, team_ids)

    @classmethod
    def intersect_ids(cls, ids, id_pool):
        common_ids = list(set(ids).intersection(id_pool)) if '*' not in id_pool else ids
        return common_ids

    @classmethod
    def is_blocked(cls, resource_id, referred_resources_graph, referred_acls_graph, action, user_ids, team_ids, crawled_object_ids):
        resource = referred_resources_graph[resource_id]
        acl = referred_acls_graph.get(resource_id, None)
        blocked_agents = cls.get_id_cards_for_action(acl, action, cls.BLOCK)

        if cls.intersect_ids(user_ids, blocked_agents.get('users', [])):
            return True
        if cls.intersect_ids(team_ids, blocked_agents.get('teams', [])):
            return True

        referred_obj_ids = cls.referred_object_ids(resource)
        if not referred_obj_ids:
            return False

        un_crawled_referred_obj_ids = list(set(referred_obj_ids).difference(crawled_object_ids))
        crawled_object_ids.extend(un_crawled_referred_obj_ids)

        for ro_id in un_crawled_referred_obj_ids:
            if cls.is_blocked(
                    ro_id, referred_resources_graph, referred_acls_graph, action, user_ids, team_ids, crawled_object_ids):
                return True

        return False

    @classmethod
    def is_effectively_granted(
            cls, object_id, referred_resources_graph, referred_acls_graph,
            action, user_ids, team_ids, crawled_object_ids):

        crawled_object_ids = crawled_object_ids[:]
        obj = referred_resources_graph[object_id]
        acl = referred_acls_graph.get(object_id, None)

        granted_agents = cls.get_id_cards_for_action(acl, action, cls.GRANT)
        matched_granted_user_ids = cls.intersect_ids(user_ids, granted_agents.get('users', []))
        matched_granted_team_ids = cls.intersect_ids(team_ids, granted_agents.get('teams', []))

        if len(matched_granted_user_ids) or len(matched_granted_team_ids):
            return True, matched_granted_user_ids + matched_granted_team_ids, crawled_object_ids

        explicitly_revoked_agents = cls.get_id_cards_for_action(acl, action, cls.REVOKE)
        revoked_user_ids = cls.intersect_ids(user_ids, explicitly_revoked_agents.get('users', []))
        revoked_team_ids = cls.intersect_ids(team_ids, explicitly_revoked_agents.get('teams', []))

        residual_team_ids = list(set(team_ids).difference(revoked_team_ids))
        residual_user_ids = list(set(user_ids).difference(revoked_user_ids))

        referred_obj_ids = cls.referred_object_ids(obj)
        if not referred_obj_ids:
            return False, None, crawled_object_ids

        un_crawled_referred_obj_ids = list(set(referred_obj_ids).difference(crawled_object_ids))
        crawled_object_ids.extend(un_crawled_referred_obj_ids)

        for roid in un_crawled_referred_obj_ids:
            ro_is_explicitly_granted, ro_matched_granted_ids, crawled_object_ids = cls.is_effectively_granted(
                roid, referred_resources_graph, referred_acls_graph,
                action, residual_user_ids[:], residual_team_ids[:], crawled_object_ids
            )

            if ro_is_explicitly_granted:
                return True, ro_matched_granted_ids, crawled_object_ids

        return False, None, crawled_object_ids

    @classmethod
    def resolve_permission_on_res_with_referred_graph(
            cls, resource_id, referred_resources_graph, referred_acls_graph, action, user_id, team_ids=None):
        team_ids = team_ids or []

        if cls.is_blocked(
                resource_id, referred_resources_graph, referred_acls_graph, action, [user_id], team_ids, [resource_id]):
            return False

        is_effectively_granted, matched_granted_ids, crawled_obj_ids = cls.is_effectively_granted(
            resource_id, referred_resources_graph, referred_acls_graph, action, [user_id], team_ids or [], [resource_id]
        )

        return is_effectively_granted

    @classmethod
    def get_resolved_permissions_for_resources(cls, resource_colln, acl_svc, resources, actions, user_id, team_ids, resource_graph_cache=None, acls_cache=None):
        resolved_permissions_list = []

        referred_resources_graph = resource_graph_cache if resource_graph_cache is not None else {}
        referred_resources_graph.update(dict((resource['_id'], resource) for resource in resources))

        referred_acls_graph = acls_cache if acls_cache is not None else {}
        cls._ensure_objects_in_graph(acl_svc, [r['_id'] for r in resources], referred_acls_graph)

        resources_referred_ids_cumulative_set = set()

        for resource in resources:
            referred_resources_graph, referred_ids_cumulative_set = cls.get_referred_resources_graphs(
                resource_colln, resource, referred_resources_graph, in_place=True
            )
            resources_referred_ids_cumulative_set.update(referred_ids_cumulative_set)

        cls._ensure_objects_in_graph(acl_svc, list(resources_referred_ids_cumulative_set), referred_acls_graph)

        for resource in resources:
            '''
            if resource['_id'] not in referred_acls_graph:
                resolved_permissions_list.append(None)
                continue
            '''
            resolved_permissions = {"jsonClass": "ResolvedPermissions"}
            for action in actions:
                if action not in cls.ACTIONS:
                    continue
                resolved_permissions[action] = cls.resolve_permission_on_res_with_referred_graph(
                    resource['_id'], referred_resources_graph, referred_acls_graph, action, user_id, team_ids=team_ids)
            resolved_permissions_list.append(resolved_permissions)

        return resolved_permissions_list

    @classmethod
    def get_resolved_permissions(
            cls, resource_colln, acl_svc, resource, actions,
            user_id, team_ids,
            resource_graph_cache=None, acls_cache=None):
        return cls.get_resolved_permissions_for_resources(
            resource_colln, acl_svc, [resource], actions,
            user_id, team_ids,
            resource_graph_cache=resource_graph_cache, acls_cache=acls_cache)[0]

    @classmethod
    def attach_resolved_permissions(cls, resource_colln, acl_svc, resources, actions, user_id, team_ids):
        resolved_permissions_list = cls.get_resolved_permissions_for_resources(
            resource_colln, acl_svc, resources, actions, user_id, team_ids)
        for i, r in enumerate(resources):
            r['resolvedPermissions'] = resolved_permissions_list[i]


'''
helpers for creationg permissions, agents set templates
'''


class AclsGenerator(object):

    @classmethod
    def get_agent_ids_template(cls):
        return {
            "jsonClass": "AgentIds",
            "type": "foaf:Agent",
            "users": [],
            "teams": []
        }

    @classmethod
    def get_action_control_template(cls):
        access_control = {"jsonClass": "AccessControl", "type": "vv:ActionControl"}
        for control in PermissionResolver.CONTROLS:
            access_control[control] = cls.get_agent_ids_template()
        return access_control

    @classmethod
    def get_acl_template(cls):
        acl = {"jsonClass": "Acl", "type": "vv:Acl"}
        for action in PermissionResolver.ACTIONS:
            acl[action] = cls.get_action_control_template()
        return acl

    @classmethod
    def add_to_agent_ids(cls, acl_json, actions, control, user_ids=None, team_ids=None):
        if control not in PermissionResolver.CONTROLS:
            return
        for action in actions:
            if action not in PermissionResolver.ACTIONS:
                continue
            agent_ids = acl_json[action][control]
            agent_ids['users'] = list(set(agent_ids['users'] + (user_ids or [])))
            agent_ids['teams'] = list(set(agent_ids['teams'] + (team_ids or [])))

    @classmethod
    def remove_from_agent_ids(cls, acl_json, actions, control, user_ids=None, team_ids=None):
        if control not in PermissionResolver.CONTROLS:
            return
        for action in actions:
            if action not in PermissionResolver.ACTIONS:
                continue
            agent_ids = acl_json[action][control]
            agent_ids['users'] = list(set(agent_ids['users']).difference(user_ids or []))
            agent_ids['teams'] = list(set(agent_ids['teams']).difference(team_ids or []))
