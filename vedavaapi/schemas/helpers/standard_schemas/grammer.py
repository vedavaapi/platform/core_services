schemas = [
    {
        "jsonClass": "VVSchema",
        "name": "MorphAnnotation",
        "properties": {
            "body": {
                "type": "object",
                "properties": {
                    "dhatu": {
                        "type": "string"
                    },
                    "gana": {
                        "type": "string"
                    },
                    "lakara": {
                        "type": "string"
                    },
                    "level": {
                        "type": ["number", "string"]
                    },
                    "padi": {
                        "type": "string"
                    },
                    "prayoga": {
                        "type": "string"
                    },
                    "purusha": {
                        "type": "string"
                    },
                    "root": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    },
                    "vachana": {
                        "type": "string"
                    },
                    "linga": {
                        "type": "string"
                    },
                    "pratyaya": {
                        "type": "string"
                    },
                    "vibhakti": {
                        "type": ["number", "string"]
                    }
                },
                "_display": [
                    "dhatu", "gana", "lakara", "level", "padi", "prayoga", "purusha",
                    "root", "type", "vachana", "linga", "pratyaya", "vibhakti"
                ],
            },
        },
        "source": "Annotation",
        "_display": ["target", "body", "jsonClassLabel"],
        "required": ["target", "body"]
    }
]
