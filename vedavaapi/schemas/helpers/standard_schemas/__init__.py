from . import annotations, books, data, others, representations, selectors, structs, grammer, discourse, logidocs


def get_schemas():
    all_modules = [annotations, books, data, others, representations, selectors, structs, grammer, discourse, logidocs]
    schemas = []

    for module in all_modules:
        schemas.extend(module.schemas)

    return dict((s['name'], s) for s in schemas)
