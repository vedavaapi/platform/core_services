import bcrypt
import time

from authlib.oauth2.rfc6749.models import ClientMixin, AuthorizationCodeMixin, TokenMixin
from flask import g
from vedavaapi.objectdb.mydb import MyDbCollection


class OAuth2BaseModel(object):

    @classmethod
    def cast(cls, obj):
        if obj is None:
            return
        obj.__class__ = cls


class UserModel(OAuth2BaseModel):

    def __init__(self, user_json):
        self.json = user_json

    def get_user_id(self):
        return self.json['_id']

    def check_password(self, password):
        if 'password' not in self.json:
            return False
        if bcrypt.checkpw(password.encode('utf-8'), self.json['password'].encode('utf-8')):
            return True
        return False


class OAuth2ClientModel(OAuth2BaseModel, ClientMixin):

    scope = 'vedavaapi.root'  # TODO

    def __init__(self, client_json):
        self.json = client_json

    @property
    def client_metadata(self):
        keys = [
            'redirect_uris', 'token_endpoint_auth_method', 'grant_types',
            'response_types', 'client_name', 'client_uri', 'logo_uri',
            'scope', 'contacts', 'tos_uri', 'policy_uri', 'jwks_uri', 'jwks',
        ]
        metadata = {k: self.json[k] for k in keys if k in self.json}
        return metadata

    @client_metadata.setter
    def client_metadata(self, value):
        for k in value:
            if k in self.json:
                self.json[k] = value[k]

    @property
    def client_info(self):
        return dict(
            client_id=self.json.get('client_id', None),
            client_secret=self.json.get('client_secret', None),
            client_id_issued_at=self.json.get('issued_at', None),
            client_secret_expires_at=self.json.get('expires_at', None)
        )

    @classmethod
    def get_client_selector_doc(cls, _id=None, client_id=None):
        if _id is not None:
            selector_doc = {"jsonClass": "OAuth2Client", "_id": _id}

        elif client_id is not None:
            selector_doc = {"jsonClass": "OAuth2Client", "client_id": client_id}

        else:
            selector_doc = None

        return selector_doc

    @classmethod
    def get_client(cls, oauth_colln, _id=None, client_id=None, projection=None):
        client_selector_doc = cls.get_client_selector_doc(_id=_id, client_id=client_id)

        if client_selector_doc is None:
            return None

        if projection is not None:
            if 0 in projection.values():
                projection.pop('jsonClass', None)
            else:
                projection.update({"jsonClass": 1})

        client_json = oauth_colln.find_one(client_selector_doc, projection=projection)
        if not client_json:
            return None

        return OAuth2ClientModel(client_json)

    @classmethod
    def get_underscore_id(cls, oauth_colln, client_id):
        client = cls.get_client(oauth_colln, client_id=client_id, projection={"_id": 1})
        return client.json['_id'] if client else None

    @classmethod
    def client_exists(cls, oauth_colln, _id=None, client_id=None):
        projection = {"_id": 1, "jsonClass": 1}
        client = cls.get_client(oauth_colln, _id=_id, client_id=client_id, projection=projection)

        return client is not None

    @classmethod
    def insert_new_client(cls, oauth_colln, client):
        client_id = oauth_colln.insert_one(client.client_json)
        return client_id

    def get_client_id(self):
        return self.json['client_id']

    def get_default_redirect_uri(self):
        if 'redirect_uris' in self.json:
            return self.json['redirect_uris'][0]

    def check_redirect_uri(self, redirect_uri):
        if 'redirect_uris' not in self.json:
            return False
        return redirect_uri in self.json['redirect_uris']

    def get_allowed_scope(self, scope):
        if not scope:
            return ''
        allowed = set(self.scope.split())
        return ' '.join([s for s in scope.split() if s in allowed])

    def has_client_secret(self):
        return 'client_secret' in self.json

    def check_token_endpoint_auth_method(self, method):
        if 'token_endpoint_auth_method' not in self.json:
            return False
        return self.json['token_endpoint_auth_method'] == method

    def check_response_type(self, response_type):
        if 'response_types' not in self.json:
            return False
        return response_type in self.json['response_types']

    def check_grant_type(self, grant_type):
        if 'grant_types' not in self.json:
            return False
        return grant_type in self.json['grant_types']

    def check_requested_scopes(self, scopes):
        if 'scope' not in self.json:
            return False
        allowed = set(self.json['scope'].split())
        return allowed.issuperset(set(scopes))

    def check_client_secret(self, client_secret):
        if 'client_secret' not in self.json:
            return False
        return self.json['client_secret'] == client_secret


class OAuth2AuthorizationCodeModel(OAuth2BaseModel, AuthorizationCodeMixin):

    def __init__(self, auth_code_json):
        self.json = auth_code_json

    def is_expired(self):
        return self.json['auth_time'] + 300 < time.time()

    def get_redirect_uri(self):
        return self.json.get('redirect_uri', None)

    def get_scope(self):
        return self.json.get('scope', None)

    def get_auth_time(self):
        return self.json.get('auth_time', None)


class OAuth2TokenModel(OAuth2BaseModel, TokenMixin):

    def __init__(self, token_json):
        self.json = token_json

    def get_scope(self):
        return self.json.get('scope', None)

    def get_expires_in(self):
        return self.json.get('expires_in', None)

    def get_expires_at(self):
        return self.json['issued_at'] + self.json['expires_in']

    def is_refresh_token_expired(self):
        expires_at = self.json['issued_at'] + self.json['expires_in'] * 2
        return expires_at < time.time()


def get_model(colln, query_doc, model_cls, projection=None):
    item_json = colln.find_one(query_doc, projection=projection)
    if item_json is None:
        return None
    return model_cls(item_json)


def create_query_client_func(oauth_colln):
    """

    :type oauth_colln: MyDbCollection
    :return:
    """

    def query_client(client_id):
        query_doc = {
            "jsonClass": "OAuth2Client",
            "client_id": client_id
        }
        return get_model(oauth_colln, query_doc, model_cls=OAuth2ClientModel)

    return query_client


def create_save_token_func(oauth_colln):
    """

    :type oauth_colln: MyDbCollection
    :return:
    """

    def save_token(token, request):
        if request.user:
            user_id = request.user.get_user_id()
        else:
            user_id = None

        client = request.client
        item_json = {
            "jsonClass": 'OAuth2Token',
            "client_id": client.get_client_id(),
            "user_id": user_id,
            "issued_at": time.time()
        }
        item_json.update(dict(**token))
        oauth_colln.insert_one(item_json)

    return save_token


def create_query_token_func(oauth_colln):

    def query_token(token, token_type_hint, client):

        query_doc = {
            "jsonClass": "OAuth2Token",
            "client_id": client.json['client_id'],
            "revoked": False
        }
        if token_type_hint == 'access_token':
            query_doc.update({"access_token": token})
            return get_model(oauth_colln, query_doc, model_cls=OAuth2TokenModel)
        elif token_type_hint == 'refresh_token':
            query_doc.update({"refresh_token": token})
            return get_model(oauth_colln, query_doc, model_cls=OAuth2TokenModel)

        # without token_type_hint
        query_doc.update({"access_token": token})
        item = get_model(oauth_colln, query_doc, model_cls=OAuth2TokenModel)
        if item is not None:
            return item

        query_doc.pop('access_token', None)
        query_doc.update({"refresh_token": token})
        return get_model(oauth_colln, query_doc, model_cls=OAuth2TokenModel)

    return query_token


def create_token_introspection_endpoint(oauth_colln, users_colln):
    from authlib.oauth2.rfc7662 import IntrospectionEndpoint
    query_token = create_query_token_func(oauth_colln)

    class _IntrospectionEndpoint(IntrospectionEndpoint):

        #  TODO

        def query_token(self, token, token_type_hint, client):
            return query_token(token, token_type_hint, client)

        def introspect_token(self, token):
            token_json = token.json.copy()
            client_id = token_json['client_id']
            # print(token, client_id)
            user_id = token_json.get('user_id', None)
            if not user_id:
                client_json = g.oauth_colln.find_one(
                    {"client_id": client_id}, projection={"user_id": 1, "creator": 1})
                user_id = client_json.get('user_id', client_json['creator'])

            user_json = users_colln.find_one(
                {"_id": user_id, "jsonClass": "User"}, projection={"_id": 1, "email": 1}
            )

            from ..agents_helpers import teams_helper
            team_ids = [
                team['_id'] for team in teams_helper.get_user_teams(
                    users_colln, token_json['user_id'], teams_projection={"_id": 1}
                )
            ]

            return {
                'active': True,
                'client_id': client_id,
                'token_type': token_json['token_type'],
                'username': user_json['email'],
                'team_ids': team_ids,
                'scope': token_json.get('scope', None),
                'sub': user_id,
                'aud': client_id,
                #  'iss': None,
                'exp': token_json['expires_at'],
                'iat': token_json['issued_at']
            }

    return _IntrospectionEndpoint


def create_revocation_endpoint(oauth_colln):
    """

    :type oauth_colln: MyDbCollection
    :return:
    """
    from authlib.oauth2.rfc7009 import RevocationEndpoint
    query_token = create_query_token_func(oauth_colln)

    class _RevocationEndpoint(RevocationEndpoint):
        def query_token(self, token, token_type_hint, client):
            return query_token(token, token_type_hint, client)

        def revoke_token(self, token):
            # noinspection PyProtectedMember
            selector_doc = {
                "jsonClass": "OAuth2Token",
                "_id": token.json['_id']
            }
            update_toc = {
                "revoked": True
            }
            oauth_colln.update_one(selector_doc, update_toc)

    return _RevocationEndpoint


def create_bearer_token_validator(oauth_colln):
    """

    :type oauth_colln: MyDbCollection
    :return:
    """
    from authlib.oauth2.rfc6750 import BearerTokenValidator

    class _BearerTokenValidator(BearerTokenValidator):

        def authenticate_token(self, token_string):
            query_doc = {
                "jsonClass": "OAuth2Token",
                "access_token": token_string
            }
            return get_model(oauth_colln, query_doc, model_cls=OAuth2TokenModel)

        def request_invalid(self, request):
            return False

        def token_revoked(self, token):
            return token.json.get('revoked', False)

    return _BearerTokenValidator
