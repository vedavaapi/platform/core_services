from .data import Text

schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "CommentAnnotation",
        "properties": {
        },
        "source": "TextAnnotation"
    },

    {
        "jsonClass": "VVSchema",
        "name": "RelationAnnotation",
        "properties": {
            "target": {
                "type": "array",
                "items": {
                    "type": "string"
                },
                "maxItems": 2,
                "minItems": 2
            },
            "directionality": {
                "enum": [
                    "uni",
                    "none",
                    "bi"
                ]
            },
            "target_labels": {
                "type": "object",
                "properties": {
                    "origin": {
                        "type": "string"
                    },
                    "destination": {
                        "type": "string"
                    }
                },
                "_display": ["origin", "destination"]
            }
        },
        "_display": [
            "directionality"
        ],
        "source": "Annotation"
    },

    {
        "jsonClass": "VVSchema",
        "name": "SequenceAnnotation",
        "properties": {
            "body": {
                "properties": {
                    "members": {
                        "type": "array",
                        "items": {
                            "properties": {
                                "index": {
                                    "type": [
                                        "string",
                                        "number"
                                    ]
                                },
                                "resource": {
                                    "type": "string"
                                }
                            }
                        }
                    },
                    "aggrContentGenerator": {
                        "properties": {
                            "delimiter": {
                                "type": "string"
                            }
                        }
                    },
                    "indexProperties": {
                        "properties": {
                            "indexPartsRegex": {
                                "type": "string"
                            },
                            "allNumeral": {
                                "type": "boolean"
                            },
                            "indexPartsMaxLengths": {
                                "type": "array",
                                "items": {
                                    "type": "integer",
                                    "minValue": 1
                                },
                                "minItems": 1
                            },
                            "padChar": {
                                "type": "string",
                                "pattern": "^.$",
                                "default": "0"
                            }
                        },
                        "required": [
                            "indexPartsRegex",
                            "allNumeral",
                            "indexPartsMaxLength"
                        ]
                    }
                },
                "_display": [
                    "jsonClassLabel",
                    "members"
                ]
            },
            "canonical": {
                "type": "string"
            }
        },
        "_primary_keys": [
            [
                "target",
                "canonical"
            ]
        ],
        "required": [
            "body",
            "target"
        ],
        "source": "Annotation",
        "_iiif_model": {
            "implements": [
                "sequence"
            ],
            "data_fields": {
                "label": "canonical"
            },
            "canvas_links": {
                "fields": [
                    "body.members"
                ]
            },
            "index_props_field": "indexProperties"
        }
    },

    {
        "jsonClass": "VVSchema",
        "name": "SpaceAnnotation",
        "properties": {
            "body": {
                "type": "object",
                "properties": {
                    "spaceType": {
                        "enum": [
                            "hole",
                            "picture",
                            "redacted",
                            "text",
                            "line",
                            "decorator"
                        ]
                    },
                    "content": {
                        "type": "string"
                    }
                }
            }
        },
        "source": "Annotation"
    },

    {
        "jsonClass": "VVSchema",
        "name": "TextAnnotation",
        "properties": {
            "type": {
                "enum": [
                    "oa:Annotation"
                ]
            },
            "body": {
              "items": Text.copy(),
              "minItems": 1,
              "type": "array"
            }
        },
        "required": [
            "body"
        ],
        "source": "Annotation"
    },

    {
        "jsonClass": "VVSchema",
        "name": "TranscriptAnnotation",
        "properties": {
        },
        "source": "TextAnnotation"
    }

]
