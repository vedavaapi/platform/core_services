
Text = {
    "jsonClass": "VVSchema",
    "name": "Text",
    "properties": {
        "type": {
            "enum": [
                "dctypes:Text"
            ]
        },
        "chars": {
            "title": "text content",
            "type": "string"
        },
        "language": {
            "type": "string",
        },
        "script": {
            "type": "string",
        },
        "mimetype": {
            "type": "string"
        },
        "contentType": {
            "type": "string"
        }
    },
    "required": [
        "chars", "jsonClass"
    ],
    "_display": [
        "chars",
        "language",
        "script",
        "jsonClass"
    ],
    "source": "JsonObject"
}

schemas = [

    {
        "jsonClass": "VVSchema",
        "name": "DataSet",
        "description": "http://www.dublincore.org/specifications/dublin-core/dcmi-terms/2012-06-14/#dcmitype-Dataset",
        "properties": {
            "type": {
                "enum": [
                    "dctypes:DataSet"
                ]
            }
        },
        "source": "OOLData"
    },

    {
        "jsonClass": "VVSchema",
        "name": "Image",
        "description": "http://www.dublincore.org/specifications/dublin-core/dcmi-terms/2012-06-14/#dcmitype-Image",
        "properties": {
            "type": {
                "enum": [
                    "dctypes:Image"
                ]
            }
        },
        "source": "OOLData"
    },

    {
        "jsonClass": "VVSchema",
        "name": "InteractiveResource",
        "description": "http://www.dublincore.org/specifications/dublin-core/dcmi-terms/2012-06-14/#dcmitype-InteractiveResource",
        "properties": {
            "type": {
                "enum": [
                    "dctypes:InteractiveResource"
                ]
            }
        },
        "source": "OOLData"
    },

    {
        "jsonClass": "VVSchema",
        "name": "MovingImage",
        "description": "http://www.dublincore.org/specifications/dublin-core/dcmi-terms/2012-06-14/#dcmitype-MovingImage",
        "properties": {
            "type": {
                "enum": [
                    "dctypes:MovingImage"
                ]
            }
        },
        "source": "Image"
    },

    {
        "jsonClass": "VVSchema",
        "name": "Sound",
        "description": "http://www.dublincore.org/specifications/dublin-core/dcmi-terms/2012-06-14/#dcmitype-Sound",
        "properties": {
            "type": {
                "enum": [
                    "dctypes:Sound"
                ]
            }
        },
        "source": "OOLData"
    },

    {
        "jsonClass": "VVSchema",
        "name": "StillImage",
        "description": "http://www.dublincore.org/specifications/dublin-core/dcmi-terms/2012-06-14/#dcmitype-StillImage",
        "properties": {
            "type": {
                "enum": [
                    "dctypes:StillImage"
                ]
            }
        },
        "source": "Image"
    },

    Text,
]
