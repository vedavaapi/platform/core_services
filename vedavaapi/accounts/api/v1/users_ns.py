import json
from collections import OrderedDict

from flask import g
import flask_restx
from jsonschema import ValidationError
from vedavaapi.common.helpers.args_parse_helper import parse_json_args

from vedavaapi.acls.permissions_helper import PermissionResolver
from vedavaapi.accounts.agents_helpers import teams_helper
from vedavaapi.objectdb.helpers import ObjModelException, projection_helper, objstore_helper

from vedavaapi.common.helpers.api_helper import jsonify_argument, check_argument_type, error_response, abort_with_error_response
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from . import api
from ...agents_helpers import users_helper

users_ns = api.namespace('users', path='/users', description='users namespace')


def _validate_projection(projection):
    try:
        projection_helper.validate_projection(projection)
    except ObjModelException as e:
        error = error_response(message=e.message, code=e.http_response_code)
        abort_with_error_response(error)


def get_requested_agents(args, colln, user_id, team_ids, filter_doc=None):
    json_parse_directives = {
        "selector_doc": {
            "allowed_types": (dict,), "default": {}
        },
        "projection": {
            "allowed_types": (dict,), "allow_none": True, "custom_validator": _validate_projection
        },
        "sort_doc": {
            "allowed_types": (dict, list), "allow_none": True
        }
    }

    args = parse_json_args(args, json_parse_directives)

    selector_doc = args['selector_doc']
    if filter_doc is not None:
        selector_doc.update(filter_doc)
    projection = args['projection']
    sort_doc = args['sort_doc']

    ops = OrderedDict()
    if sort_doc is not None:
        ops['sort'] = [sort_doc]
    if args.get('start', None) is not None and args.get('count', None) is not None:
        ops['skip'] = [args['start']]
        ops['limit'] = [args['count']]

    try:
        count = colln.count(selector_doc)
        resource_repr_jsons = objstore_helper.get_read_permitted_resources(
            colln, g.acl_svc, user_id, team_ids, selector_doc,
            projection=projection, ops=ops)
        response_json = {
            "items": resource_repr_jsons,
            "total_count": count,
            "selector_doc": selector_doc
        }
        if args['start']:
            response_json['start'] = args['start']
        if args['count']:
            response_json['count'] = args['count']

        return response_json
    except (TypeError, ValueError):
        error = error_response(message='arguments to operations seems invalid', code=400)
        abort_with_error_response(error)


@users_ns.route('')
class Users(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument('selector_doc', location='args', type=str, default='{}')
    get_parser.add_argument('projection', location='args', type=str)
    get_parser.add_argument('start', location='args', type=int)
    get_parser.add_argument('count', location='args', type=int)
    get_parser.add_argument('sort_doc', location='args', type=str)
    get_parser.add_argument('return_count', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    post_parser = api.parser()
    post_parser.add_argument('user_json', location='form', type=str, required=True)
    post_parser.add_argument('initial_team_id', location='form', type=str, required=False, default=None)
    post_parser.add_argument('return_projection', location='form', type=str)
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_parser = api.parser()
    delete_parser.add_argument('user_ids', location='form', type=str, required=True)
    delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @users_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):

        '''
        if not PermissionResolver.resolve_permission(
                g.all_users_team_json, PermissionResolver.READ,
                current_token.user_id, current_token.team_ids, g.users_colln, g.acl_svc):
            return error_response(message='Permission denied.', code=403)
        '''

        args = self.get_parser.parse_args()

        projection = jsonify_argument(args.get('projection', None), key='projection')
        check_argument_type(projection, (dict,), key='projection', allow_none=True)
        _validate_projection(projection)
        projection = projection_helper.modified_projection(projection, mandatory_attrs=["_id", "jsonClass"])

        args_copy = args.copy()
        args_copy.pop('projection', None)
        response_json = get_requested_agents(
            args_copy, g.users_colln, current_token.user_id, current_token.team_ids, filter_doc={"jsonClass": "User"})

        should_return_count = json.loads(args.get('return_count', 'false'))
        if should_return_count:
            return {"count": len(response_json['items'])}, 200

        for item in response_json['items']:
            users_helper.project_user_json(item, projection=projection, in_place=True)
        return response_json

    @users_ns.expect(post_parser, validate=True)
    @require_oauth(token_required=True)
    def post(self):
        args = self.post_parser.parse_args()

        user_json = jsonify_argument(args['user_json'], key='user_json')  # type: dict
        check_argument_type(user_json, (dict, ), key='user_json')

        initial_team_id = args.get('initial_team_id', None) or g.initial_agents.all_users_team_id
        initial_team_json = g.users_colln.find_one({"_id": initial_team_id, "jsonClass": 'Team'})
        if initial_team_json is None:
            return error_response(message='initial_team with _id {} does not exists'.format(initial_team_id), code=400)

        return_projection = jsonify_argument(args.get('return_projection', None), key='return_projection')
        check_argument_type(return_projection, (dict, ), key='return_projection', allow_none=True)
        _validate_projection(return_projection)
        return_projection = projection_helper.modified_projection(
            return_projection, mandatory_attrs=['_id', 'jsonClass'])

        if not PermissionResolver.resolve_permission(
                initial_team_json, PermissionResolver.UPDATE_CONTENT,
                current_token.user_id, current_token.team_ids, g.users_colln, g.acl_svc):
            return error_response(message='not authorized to create user in this initial team', code=403)

        try:
            new_user_id = users_helper.create_new_user(
                g.users_colln, g.acl_svc, user_json, g.schema_validator,
                initial_agents=g.initial_agents, initial_team_id=initial_team_id, password_required=False
            )
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except ValidationError as e:
            return error_response(message='invalid schema for user_json', code=403, details={"error": str(e)})

        new_user_json = g.users_colln.get(new_user_id, projection=return_projection)
        return new_user_json

    @users_ns.expect(delete_parser, validate=True)
    @require_oauth()
    def delete(self):
        args = self.delete_parser.parse_args()

        user_ids = jsonify_argument(args['user_ids'])
        check_argument_type(user_ids, (list,))

        ids_validity = False not in [isinstance(_id, str) for _id in user_ids]
        if not ids_validity:
            return error_response(message='ids should be strings', code=404)

        delete_report = []

        for user_id in user_ids:
            deleted, deleted_res_ids = objstore_helper.delete_tree(
                g.users_colln, g.acl_svc, None, user_id, current_token.user_id, current_token.team_ids)

            delete_report.append({
                "deleted": deleted,
                "deleted_resource_ids": deleted_res_ids
            })

        return delete_report


@users_ns.route('/<user_id>')
class UserResource(flask_restx.Resource):

    get_parser = users_ns.parser()
    get_parser.add_argument('projection', type=str, location='args')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    post_parser = users_ns.parser()
    post_parser.add_argument('update_doc', type=str, location='form', required=True)
    post_parser.add_argument('return_projection', type=str, location='form')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    @users_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, user_id):
        args = self.get_parser.parse_args()
        user_selector_doc = users_helper.get_user_selector_doc(_id=user_id)

        projection = jsonify_argument(args.get('projection', None), key='projection')
        check_argument_type(projection, (dict,), key='projection', allow_none=True)
        _validate_projection(projection)
        projection = projection_helper.modified_projection(projection, mandatory_attrs=["_id", "jsonClass"])

        try:
            user_json = objstore_helper.get_resource(
                g.users_colln, g.acl_svc, user_selector_doc,
                current_token.user_id, current_token.team_ids)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        if not user_json['resolvedPermissions'][PermissionResolver.READ]:
            return error_response(message='permission denied', code=403)

        projected_user_json = users_helper.project_user_json(user_json, projection=projection)
        return projected_user_json

    @users_ns.expect(post_parser, validate=True)
    @require_oauth()
    def post(self, user_id):

        args = self.post_parser.parse_args()
        user_selector_doc = users_helper.get_user_selector_doc(_id=user_id)

        update_doc = jsonify_argument(args['update_doc'], key='update_doc')
        check_argument_type(update_doc, (dict,), key='update_doc')
        if '_id' not in update_doc:
            update_doc['_id'] = user_id
        if 'jsonClass' not in update_doc:
            update_doc['jsonClass'] = 'User'

        if update_doc['_id'] != user_id:
            return error_response(message='invalid user_id', code=403)
        if update_doc['jsonClass'] != 'User':
            return error_response(message='invalid jsonClass', code=403)

        return_projection = jsonify_argument(args.get('return_projection', None), key='return_projection')
        check_argument_type(return_projection, (dict,), key='return_projection', allow_none=True)
        _validate_projection(return_projection)
        return_projection = projection_helper.modified_projection(
            return_projection, mandatory_attrs=['_id', 'jsonClass'])

        try:
            updated_user_id = objstore_helper.update_resource(
                g.users_colln, g.acl_svc, update_doc, current_token.user_id, current_token.team_ids, g.schema_validator,
                not_allowed_attributes=['externalAuthentications', 'password'])
            if updated_user_id is None:
                raise ObjModelException('user not exist', 404)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)
        except (ValueError, ValidationError, TypeError) as e:
            return error_response(message='schema validation error', code=403, details={"error": str(e)})

        user_json = g.users_colln.find_one(user_selector_doc, projection=return_projection)
        return user_json


@users_ns.route('/<user_id>/teams')
class UserTeams(flask_restx.Resource):

    get_parser = users_ns.parser()
    get_parser.add_argument('teams_projection', type=str, location='args', default=None, help='projection for each team')
    get_parser.add_argument('return_count', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    @users_ns.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, user_id):

        args = self.get_parser.parse_args()
        user_selector_doc = users_helper.get_user_selector_doc(_id=user_id)

        teams_projection = jsonify_argument(args.get('teams_projection', None), key='teams_projection')
        check_argument_type(teams_projection, (dict, ), allow_none=True)
        _validate_projection(teams_projection)

        user_json = g.users_colln.find_one(user_selector_doc, projection={"_id": 1, "permissions": 1})
        if not user_json:
            return error_response(message='user not found', code=404)

        if not PermissionResolver.resolve_permission(
                user_json, PermissionResolver.READ,
                current_token.user_id, current_token.team_ids, g.users_colln, g.acl_svc):
            return error_response(message='permission denied', code=403)

        user_team_jsons = teams_helper.get_user_teams(g.users_colln, user_id, teams_projection=None)
        PermissionResolver.attach_resolved_permissions(
            g.users_colln, g.acl_svc, user_team_jsons,
            PermissionResolver.ACTIONS, user_id, [t['_id'] for t in user_team_jsons])

        permitted_user_team_jsons = [t for t in user_team_jsons if t['resolvedPermissions']['read']]
        for t in permitted_user_team_jsons:
            projection_helper.project_doc(t, teams_projection, in_place=True)

        should_return_count = json.loads(args.get('return_count', 'false'))
        if should_return_count:
            return {"count": len(permitted_user_team_jsons)}, 200

        return permitted_user_team_jsons, 200
