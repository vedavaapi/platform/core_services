import flask_restx
from flask import Blueprint


api_blueprint_v1 = Blueprint('accounts'+'_v1', __name__, static_folder='static', static_url_path='/static')


api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Vedavaapi Accounts',
    description='Vedavaapi Agents and OAuth management api',
    doc='/docs')


from . import authorization_ns, clients_ns
from . import setup_ns, me_ns, teams_ns, users_ns
