import uuid

import bcrypt

from vedavaapi.objectdb.helpers import ObjModelException, projection_helper, objstore_helper


def get_client_selector_doc(_id=None, client_id=None):
    if _id is not None:
        selector_doc = {"jsonClass": "OAuth2Client", "_id": _id}

    elif client_id is not None:
        selector_doc = {"jsonClass": "OAuth2Client", "client_id": client_id}

    else:
        selector_doc = None

    return selector_doc


def project_client_json(client_json, projection=None):
    client_exposed_projection = projection_helper.get_restricted_projection(projection, {"client_secret": 0})
    projected_client_json = projection_helper.project_doc(
        client_json, projection_helper.get_restricted_projection(projection, client_exposed_projection))
    return projected_client_json


"""
"""


def create_new_client(
        oauth_colln, acl_svc, client_json, client_type,
        user_id, team_ids, schema_validator, initial_agents=None):

    oauth2_master_config = oauth_colln.find_one({"jsonClass": "OAuth2MasterConfig"})
    if oauth2_master_config:
        grant_privileges_conf = oauth2_master_config['grant_privileges']

        pw_agent_set = grant_privileges_conf['password']
        allow_pw_grant = (
                user_id in pw_agent_set['users']
                or True in [team in pw_agent_set['teams'] for team in team_ids]
        )
    else:
        allow_pw_grant = False

    for k in (
            '_id', 'client_id', 'client_secret', 'token_endpoint_auth_method', 'response_types', 'scope', 'user_id'):
        if k in client_json:
            raise ObjModelException('you can\'t set {} attribute'.format(k), 403)

    if client_json['jsonClass'] != "OAuth2Client":
        raise ObjModelException('invalid jsonClass', 403)

    essential_fields = ['name', 'grant_types']
    for k in essential_fields:
        if k not in client_json:
            raise ObjModelException('{} should be supplied'.format(k), 403)

    if client_type == 'public':
        allowed_grant_types = ['implicit']
    else:
        allowed_grant_types = ['authorization_code', 'refresh_token', 'client_credentials']
        if allow_pw_grant:
            allowed_grant_types.append('password')

    if not isinstance(client_json['grant_types'], list) or not len(client_json['grant_types']):
        raise ObjModelException('grant_types should not be empty', 400)

    for grant_type in client_json['grant_types']:
        if grant_type not in allowed_grant_types:
            raise ObjModelException('{} clients cannot request grant_type {}'.format(client_type, grant_type), 403)
        if grant_type in ['authorization_code', 'implicit', 'refresh_token'] and 'redirect_uris' not in client_json:
            raise ObjModelException('redirect_uris are required for grant_type {}'.format(grant_type), 403)

    client_id = uuid.uuid4().hex

    client_json.update({
        "client_id": client_id,
        "token_endpoint_auth_method": "client_secret_post" if client_type != 'public' else 'none',
        "response_types": ["code", "token"] if client_type == 'private' else ["token"],
        "scope": "vedavaapi.root", "user_id": user_id
    })

    if client_type == 'private':
        client_secret = bcrypt.gensalt(24).decode('utf-8')
        client_json['client_secret'] = client_secret

    new_client_underscore_id = objstore_helper.create_resource(
        oauth_colln, acl_svc, client_json, user_id, team_ids, schema_validator, initial_agents=initial_agents)
    return new_client_underscore_id
