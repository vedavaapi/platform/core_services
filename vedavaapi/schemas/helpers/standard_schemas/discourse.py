from copy import deepcopy

DiscourseBody = {
    "type": "object",
    "properties": {
        "tantrayukti": {
            "type": "object",
            "title": "तन्त्रयुक्तिः",
            "allowedClasses": ["TantrayuktiTag"],
        },
        "adhikarana": {
            "type": "object",
            "title": "अधिकरण",
            "allowedClasses": ["AdhikaranaTag"],
        },
        "vyakhyaana": {
            "type": "object",
            "title": "व्याख्यान",
            "allowedClasses": ["VyakhyanaTag"],
        },
        "taatparya": {
            "type": "object",
            "title": "तात्पर्य",
            "allowedClasses": ["TaatparyaTag"],
        },
        "vishaya": {
            "type": "object",
            "title": "विषय",
            "allowedClasses": ["VishayaTag"],
        },
        "grantha": {
            "type": "object",
            "title": "ग्रन्थ",
            "allowedClasses": ["GranthaTag"],
        },
        "sambandha": {
            "type": "object",
            "title": "सम्बन्ध",
            "allowedClasses": ["SambandhaTag"],
        }
    },
    "_display": ["tantrayukti", "adhikarana", "vyakhyaana", "taatparya", "vishaya", "grantha", "sambandha"]
}


schemas = [
    {
        "jsonClass": "VVSchema",
        "name": "DiscourseAnnotation",
        "source": "Annotation",
        "properties": {
            "body": deepcopy(DiscourseBody)
        },
        "required": ["body", "target"],
        "_display": ["body"],
    },

    {
        "jsonClass": "VVSchema",
        "name": "Sambandha",
        "source": "RelationAnnotation",
        "properties": {
            "body": deepcopy(DiscourseBody)
        },
        "_display": ["body"]
    },

    {
        "jsonClass": "VVSchema",
        "name": "DiscourseTag",
        "source": "VVObject",
        "properties": {
            "subjectable_to": {
                "type": "string",
            },
            "name": {
                "type": "string",
            },
            "name_en": {
                "type": "string",
            },
            "meaning": {
                "type": "string",
            },
            "description": {
                "type": "string",
            },
        },
        "_display": ["name", "name_en", "meaning", "description", "jsonClassLabel"],
        "_primary_keys": [
            ["name"]
        ]
    },

    {
        "jsonClass": "VVSchema",
        "name": "TantrayuktiTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["तन्त्रयुक्तिः"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },

    {
        "jsonClass": "VVSchema",
        "name": "AdhikaranaTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["अधिकरण"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },

    {
        "jsonClass": "VVSchema",
        "name": "VyakhyanaTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["व्याख्यान"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },

    {
        "jsonClass": "VVSchema",
        "name": "TaatparyaTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["तात्पर्य"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },

    {
        "jsonClass": "VVSchema",
        "name": "VishayaTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["विषय"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },

    {
        "jsonClass": "VVSchema",
        "name": "GranthaTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["ग्रन्थ"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },

    {
        "jsonClass": "VVSchema",
        "name": "SambandhaTag",
        "source": "DiscourseTag",
        "properties": {
            "jsonClassLabel": {
                "type": "string",
                "enum": ["सम्बन्ध"],
            }
        },
        "_primary_keys": [
            ["name"]
        ],
    },
]
