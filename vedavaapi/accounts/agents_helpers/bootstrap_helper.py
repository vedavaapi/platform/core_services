from collections import namedtuple

from vedavaapi.objectdb.helpers import json_object_helper
from vedavaapi.acls.permissions_helper import PermissionResolver, AclsGenerator

from ..agents_helpers import users_helper, teams_helper


InitialAgents = namedtuple(
    'InitialAgents',
    ('all_users_team_id', 'root_admins_team_id', 'default_vedavaapi_console_client_id')
)


# noinspection PyUnusedLocal,PyProtectedMember
def bootstrap_initial_agents(users_colln, oauth_colln, acl_svc, initial_agents_config):

    all_users_conf = initial_agents_config['teams']['all_users']
    all_users_team_id = create_all_users_team(users_colln, acl_svc, all_users_conf['team_name'])

    root_admins_conf = initial_agents_config['teams']['root_admins']
    root_admins_team_id = create_root_admins_team(
        users_colln, acl_svc, root_admins_conf['team_name'], all_users_team_id)

    set_perms_for_all_users_team(acl_svc, all_users_team_id, root_admins_team_id)

    # bootstrap_oauth2_master_config(oauth_colln, acl_svc, root_admins_team_id)
    default_vedavaapi_console_client_id = create_default_vedavaapi_org_client(oauth_colln, acl_svc, root_admins_team_id)
    return InitialAgents(all_users_team_id, root_admins_team_id, default_vedavaapi_console_client_id)


# noinspection PyProtectedMember
def create_all_users_team(users_colln, acl_svc, team_name):
    existing_team_json = users_colln.find_one(
        teams_helper.get_team_selector_doc(_id="ALL_USERS_TEAM"), projection={"_id": 1})
    if existing_team_json is not None:
        return existing_team_json['_id']

    all_users_team_json = {
        "_id": "ALL_USERS_TEAM",
        "jsonClass": "Team",
        "type": "foaf:Agent",
        "name": team_name,
        "source": None,
        "members": [],
        "description": "all vedavaapi users",
    }
    json_object_helper.update_time(all_users_team_json)
    all_users_team_id = users_colln.insert_one(all_users_team_json).inserted_id

    acl = AclsGenerator.get_acl_template()
    AclsGenerator.add_to_agent_ids(
        acl, [PermissionResolver.READ, PermissionResolver.CREATE_CHILDREN], PermissionResolver.GRANT,
        team_ids=[all_users_team_id]
    )
    acl['_id'] = all_users_team_id
    acl_svc.update(all_users_team_id, acl, upsert=True)

    return all_users_team_id


# noinspection PyProtectedMember
def create_root_admins_team(users_colln, acl_svc, team_name, parent_team_id):
    existing_team = users_colln.find_one(
        teams_helper.get_team_selector_doc(_id="ROOT_ADMINS_TEAM"), projection={"_id": 1})
    if existing_team is not None:
        return existing_team['_id']

    root_admins_team_json = {
        "_id": "ROOT_ADMINS_TEAM",
        "jsonClass": "Team",
        "type": "foaf:Agent",
        "name": team_name,
        "source": parent_team_id,
        "members": [],
        "description": "Vedavaapi Root Admins Team",
    }
    json_object_helper.update_time(root_admins_team_json)
    root_admins_team_id = users_colln.insert_one(root_admins_team_json).inserted_id

    acl = AclsGenerator.get_acl_template()
    AclsGenerator.add_to_agent_ids(
        acl, [PermissionResolver.READ, PermissionResolver.UPDATE_CONTENT, PermissionResolver.CREATE_CHILDREN],
        PermissionResolver.GRANT, team_ids=[root_admins_team_id]
    )
    acl['_id'] = root_admins_team_id
    acl_svc.update(root_admins_team_id, acl, upsert=True)

    return root_admins_team_id


def set_perms_for_all_users_team(acl_svc, all_users_team_id, root_admins_team_id):
    acl_svc.add_to_agent_ids(
        [all_users_team_id], [PermissionResolver.UPDATE_CONTENT, PermissionResolver.UPDATE_PERMISSIONS],
        PermissionResolver.GRANT, team_ids=[root_admins_team_id]
    )


'''
def bootstrap_oauth2_master_config(oauth_colln, acl_svc, root_admins_team_id):
    existing_master_config = oauth_colln.find_one({"jsonClass": 'OAuth2MasterConfig'}, projection={"_id": 1})
    if existing_master_config:
        return existing_master_config['_id']

    pw_agent_set_json = { "jsonClass" : "AgentSet", "users" : [], "teams" :
        [root_admins_team_id] }
    grant_privileges_conf_json = {
        "jsonClass": "WrapperObject",
        "password": pw_agent_set_json
    }

    permissions_json = permissions_helper.PermissionsGenerator.get_object_permissions_template()
    permissions_helper.PermissionsGenerator.add_to_agent_sets(
        permissions_json, [PermissionResolver.READ, PermissionResolver.UPDATE_CONTENT],
        'granted', team_pids=[root_admins_team_id]
    )

    oauth2_master_config_json = {
        "jsonClass": "OAuth2MasterConfig",
        "permissions": permissions_json,
        "grant_privileges": grant_privileges_conf_json
    }
    return oauth_colln.insert_one(oauth2_master_config_json).inserted_id
'''  # TODO


def create_default_vedavaapi_org_client(oauth_colln, acl_svc, root_admins_team_id):
    client_id = 'DEFAULT_VEDAVAAPI_CLIENT'
    redirect_uris = [
        "https://apps.vedavaapi.org/prod/ui/oauth_callback",
        "https://apps.vedavaapi.org/ui/oauth_callback",
        "https://apps.vedavaapi.org/dev/ui/oauth_callback",
        "https://apps.vedavaapi.org/stage/ui/oauth_callback",
        "http://apps.vedavaapi.org/prod/ui/oauth_callback",
        "http://apps.vedavaapi.org/dev/ui/oauth_callback",
        "http://apps.vedavaapi.org/stage/ui/oauth_callback",
        "http://localhost:4200/oauth_callback",
        "http://localhost:9002/prod/ui/oauth_callback",
        "http://localhost:6002/dev/ui/oauth_callback"
    ]
    existing_client = oauth_colln.find_one({"jsonClass": "OAuth2Client", "client_id": client_id}, projection={"_id": 1})
    if existing_client:
        return existing_client['_id']

    client_json = {
        "_id": "DEFAULT_VEDAVAAPI_CLIENT",
        "jsonClass": "OAuth2Client",
        "client_id": client_id,
        "client_name": "Vedavaapi Console",
        "grant_types": ["implicit"],
        "response_types": ["code", "token"],
        "token_endpoint_auth_method": "none",
        "redirect_uris": redirect_uris,
        "scope": "vedavaapi.root"
    }

    client_id = oauth_colln.insert_one(client_json).inserted_id

    acl = AclsGenerator.get_acl_template()
    AclsGenerator.add_to_agent_ids(
        acl, PermissionResolver.ACTIONS, PermissionResolver.GRANT, team_ids=[root_admins_team_id])
    acl_svc.update(client_id, acl, upsert=True)

    return client_id


def is_root_admins_team_empty(org_name):
    from .. import VedavaapiAccounts
    accounts_service = VedavaapiAccounts.instance
    root_admins_team_id = 'ROOT_ADMINS_TEAM'
    colln = accounts_service.get_users_colln(org_name)
    root_admins_team_json = colln.find_one({"_id": root_admins_team_id}, projection={"members": 1})
    return not bool(root_admins_team_json and len(root_admins_team_json.get('members', [])))


def create_root_admin(org_name, acl_svc, email, password):
    '''if not is_root_admins_team_empty(org_name):
        return None'''

    from .. import VedavaapiAccounts
    accounts_service = VedavaapiAccounts.instance
    initial_agents = accounts_service.get_initial_agents(org_name)
    if not initial_agents:
        return None

    users_colln = accounts_service.get_users_colln(org_name)

    root_admin_id = users_helper.get_user_id(users_colln, email)
    if root_admin_id is None:
        user_json = {
            "jsonClass": "User",
            "email": email,
            "password": password
        }
        root_admin_id = users_helper.create_new_user(
            users_colln, acl_svc, user_json, None, validate_schema=False, validate_permissions=False)

    teams_helper.add_users_to_team(
        users_colln, acl_svc,
        teams_helper.get_team_selector_doc(_id=initial_agents.root_admins_team_id),
        [root_admin_id], None, None, validate_permissions=False)

    # permissions_helper.add_to_agent_set(users_colln, [initial_agents.root_admins_team_id], ObjectPermissions.ACTIONS, 'granted', user_pids=[root_admin_id], team_pids=[initial_agents.root_admins_team_id])
    return root_admin_id
