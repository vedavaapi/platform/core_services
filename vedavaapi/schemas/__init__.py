import logging

from collections import namedtuple

from vedavaapi.objectdb.mydb import MyDbCollection
from vedavaapi.common import VedavaapiService, OrgHandler
from vv_schemas import SchemaValidator

from .helpers import VVSchemas
from .helpers import bootstrap_helper


logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)


class SchemasOrgHandler(OrgHandler):
    def __init__(self, service, org_name):
        super(SchemasOrgHandler, self).__init__(service, org_name)

        '''
        self.objstore_db_config = self.dbs_config['objstore_db']
        self.objstore_db = self.store.db(self.objstore_db_config['name'])
        self.objstore_colln = self.objstore_db.get_collection(
            self.objstore_db_config['collections']['objstore']
        )
        '''
        self.objstore_colln = self.service.registry.lookup('objstore').colln(self.org_name)

        self.root_dir_path = self.store.file_store_path(
            file_store_type='data',
            base_path=''
        )
        InitialAgents = namedtuple(
            'InitialAgents',
            ('all_users_team_id', 'root_admins_team_id')
        )
        self.initial_agents = InitialAgents('ALL_USERS_TEAM', 'ROOT_ADMINS_TEAM')

    def initialize(self):
        acl_svc = self.service.registry.lookup('acls').get_acl_svc(self.org_name)
        bootstrap_helper.bootstrap_initial_schemas(self.objstore_colln, acl_svc, self.initial_agents)


class VedavaapiSchemas(VedavaapiService):

    instance = None  # type: VedavaapiSchemas

    dependency_services = ['objstore', 'acls']
    org_handler_class = SchemasOrgHandler

    title = "Vedavaapi Schemas"
    description = "schemas api"

    def __init__(self, registry, name, conf):
        super(VedavaapiSchemas, self).__init__(registry, name, conf)

    def init_service(self):
        for org_name in self.registry.org_names:
            self.get_org(org_name)  # initializes all orgs.

    def colln(self, org_name):
        return self.get_org(org_name).objstore_colln  # type: MyDbCollection

    def get_schemas(self, org_name):
        # new one each time.
        colln =  self.get_org(org_name).objstore_colln
        return VVSchemas(colln)

    def get_schema_validator(self, org_name, res_colln):
        vv_schemas = self.get_schemas(org_name)
        return SchemaValidator(vv_schemas, res_colln)
