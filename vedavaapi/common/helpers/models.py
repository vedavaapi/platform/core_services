from collections import namedtuple

from vedavaapi.objectdb.mydb import MyDbCollection
from vv_schemas import SchemaValidator
from vedavaapi.acls.acl_service import AclSvc
from vedavaapi.schemas.helpers import VVSchemas


InitialAgents = namedtuple(
        'InitialAgents',
        ('all_users_team_id', 'root_admins_team_id')
    )


class VVContextDescriptor(object):

    def __init__(
            self, objstore_colln_descr, data_dir_path,
            res_colln_descr, acl_colln_descr, tasks_colln_descr=None):
        self.objstore_colln_descr = objstore_colln_descr
        self.data_dir_path = data_dir_path
        self.res_colln_descr = res_colln_descr
        self.acl_colln_descr = acl_colln_descr
        self.tasks_colln_descr = tasks_colln_descr


class VVContext(object):

    def __init__(
            self, colln: MyDbCollection, data_dir_path,
            acl_svc: AclSvc, schema_validator: SchemaValidator,
            tasks_colln=None):
        self.colln = colln
        self.data_dir_path = data_dir_path
        self.acl_svc = acl_svc
        self.schema_validator = schema_validator
        self.initial_agents = InitialAgents('ALL_USERS_TEAM', 'ROOT_ADMINS_TEAM')
        self.tasks_colln = tasks_colln
        from vedavaapi.tasker.lib import TaskManager
        self.task_manager = TaskManager(self, tasks_colln)

    @classmethod
    def from_descriptor(cls, descriptor: VVContextDescriptor):
        objstore_colln = MyDbCollection.from_descriptor(descriptor.objstore_colln_descr)
        res_colln = MyDbCollection.from_descriptor(descriptor.res_colln_descr)
        acl_colln = MyDbCollection.from_descriptor(descriptor.acl_colln_descr)
        tasks_colln = MyDbCollection.from_descriptor(
            descriptor.tasks_colln_descr) if descriptor.tasks_colln_descr else None

        #  from vedavaapi.acls.acl_service import AclSvc
        #  from vedavaapi.schemas.helpers import VVSchemas
        from vv_schemas import SchemaValidator

        acl_svc = AclSvc(acl_colln)
        vvschemas = VVSchemas(objstore_colln)
        schema_validator = SchemaValidator(vvschemas, res_colln)
        return cls(
            res_colln, descriptor.data_dir_path, acl_svc,
            schema_validator, tasks_colln=tasks_colln)

    def get_descriptor(self):
        return VVContextDescriptor(
            MyDbCollection.get_descriptor(self.schema_validator.colln),
            self.data_dir_path,
            MyDbCollection.get_descriptor(self.colln),
            MyDbCollection.get_descriptor(self.acl_svc.colln),
            tasks_colln_descr=MyDbCollection.get_descriptor(
                self.tasks_colln) if self.tasks_colln else None
        )


class AgentContext(object):

    def __init__(self, user_id, team_ids):
        self.user_id = user_id
        self.team_ids = team_ids
