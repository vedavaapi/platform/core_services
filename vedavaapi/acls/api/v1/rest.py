import flask_restx
from flask import g

from vedavaapi.common.helpers.api_helper import error_response, get_user, get_team, get_current_org
from vedavaapi.common.helpers.args_parse_helper import parse_json_args
from vedavaapi.common.helpers.token_helper import current_token, require_oauth
from vedavaapi.objectdb.helpers import objstore_helper, ObjModelException

from ...permissions_helper import PermissionResolver
from . import api


@api.route('/<resource_id>/resolved_permissions')
class ResolvedPermissions(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    @api.expect(get_parser, validate=True)
    @require_oauth()
    def get(self, resource_id):

        try:
            resource = objstore_helper.get_resource(
                g.objstore_colln, g.acl_svc, {"_id": resource_id}, current_token.user_id, current_token.team_ids)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        return resource['resolvedPermissions'], 200


@api.route('/<resource_id>')
class Acls(flask_restx.Resource):

    post_delete_parser = api.parser()
    post_delete_parser.add_argument(
        'actions', location='form', type=str, required=True,
        help='any combination among {}'.format(str(PermissionResolver.ACTIONS))
    )
    post_delete_parser.add_argument(
        'control', location='form', type=str, required=True,
        choices=PermissionResolver.CONTROLS
    )
    post_delete_parser.add_argument('user_ids', location='form', type=str, default='[]')
    post_delete_parser.add_argument('team_ids', location='form', type=str, default='[]')
    post_delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_delete_payload_json_parse_directives = {
        "actions": {
            "allowed_types": (list, )
        },
        "user_ids": {
            "allowed_types": (list, ), "default": []
        },
        "team_ids": {
            "allowed_types": (list,), "default": []
        }
    }

    def get(self, resource_id):
        acl = g.acl_svc.get(resource_id)
        if not acl:
            return error_response(message='acl does not exist', code=404)
        return acl ## TODO

    @api.expect(post_delete_parser, validate=True)
    @require_oauth()
    def post(self, resource_id):
        current_org_name = get_current_org()
        args = parse_json_args(self.post_delete_parser.parse_args(), self.post_delete_payload_json_parse_directives)

        actions = args['actions']
        control = args['control']
        user_ids = args['user_ids']
        team_ids = args['team_ids']

        def get_user_fn(user_id, projection=None):
            return get_user(current_org_name, user_id, projection=projection)  # TODO!

        def get_team_fn(team_id, projection=None):
            return get_team(current_org_name, team_id, projection=projection)

        try:
            objstore_helper.add_to_agent_ids(
                g.objstore_colln, g.acl_svc, resource_id,
                current_token.user_id, current_token.team_ids,
                actions, control, get_user_fn, get_team_fn,
                user_ids, team_ids
            )
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        return g.acl_svc.get(resource_id)

    @api.expect(post_delete_parser, validate=True)
    @require_oauth()
    def delete(self, resource_id):
        args = parse_json_args(self.post_delete_parser.parse_args(), self.post_delete_payload_json_parse_directives)

        actions = args['actions']
        control = args['control']
        user_ids = args['user_ids']
        team_ids = args['team_ids']

        try:
            objstore_helper.remove_from_permissions_agent_set(
                g.objstore_colln, g.acl_svc, resource_id,
                current_token.user_id, current_token.team_ids,
                actions, control, user_ids=user_ids, team_ids=team_ids
            )
        except ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        return g.acl_svc.get(resource_id)
