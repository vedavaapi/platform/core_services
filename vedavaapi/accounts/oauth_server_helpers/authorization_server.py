from collections import namedtuple

from authlib.oauth2.rfc6749.grants import ImplicitGrant
from authlib.integrations.flask_oauth2.authorization_server import AuthorizationServer as _AuthorizationServer


from .models import create_query_client_func, create_save_token_func, create_revocation_endpoint, create_token_introspection_endpoint
from .grants import AuthorizationCodeGrant, RefreshTokenGrant, PasswordGrant, ClientCredentialsGrant


class AuthorizationServer(_AuthorizationServer):

    def __init__(self, config, oauth_colln, users_colln, **kwargs):
        self.oauth_colln = oauth_colln
        self.users_colln = users_colln
        WrapperApp = namedtuple('WrapperApp', ('config', ))
        app = WrapperApp(config)
        self.app = app
        query_client = create_query_client_func(self.oauth_colln)
        save_token = create_save_token_func(self.oauth_colln)

        super(AuthorizationServer, self).__init__(app=app, query_client=query_client, save_token=save_token, **kwargs)
        self.register_grants()
        self.register_endpoints()

    def register_grants(self):
        self.register_grant(AuthorizationCodeGrant)
        self.register_grant(RefreshTokenGrant)
        self.register_grant(PasswordGrant)
        self.register_grant(ImplicitGrant)
        self.register_grant(ClientCredentialsGrant)

    def register_endpoints(self):
        self.register_endpoint(create_revocation_endpoint(self.oauth_colln))
        self.register_endpoint(create_token_introspection_endpoint(self.oauth_colln, self.users_colln))
