import time

from authlib.oauth2.rfc6749 import grants
from werkzeug.security import gen_salt

from vedavaapi.objectdb.mydb import MyDbCollection

from .models import (
    get_model,
    UserModel)
from .models import (
    OAuth2AuthorizationCodeModel,
    OAuth2TokenModel
)
from ..agents_helpers import users_helper


# noinspection PyProtectedMember
class AuthorizationCodeGrant(grants.AuthorizationCodeGrant):

    def __init__(self, request, server):
        super(AuthorizationCodeGrant, self).__init__(request, server)
        self.oauth_colln = self.server.oauth_colln  # type: MyDbCollection
        self.users_colln = self.server.users_colln  # type: MyDbCollection

    def create_authorization_code(self, client, grant_user, request):
        authorization_code_doc = {
            "client_id": client.json['client_id'],
            "redirect_uri": request.redirect_uri,
            "scope": request.scope,
            "user_id": grant_user.json['_id'],
        }
        existing_item = get_model(
            self.oauth_colln, authorization_code_doc, OAuth2AuthorizationCodeModel)
        if existing_item and not existing_item.is_expired():
            return existing_item.json['code']

        item_json = {
            "jsonClass": "OAuth2AuthorizationCode",
            "code": gen_salt(48),
            "auth_time": time.time()
        }
        item_json.update(authorization_code_doc)
        self.oauth_colln.insert_one(item_json)
        return item_json['code']

    def parse_authorization_code(self, code, client):
        query_doc = {"jsonClass": "OAuth2AuthorizationCode", "code": code, "client_id": client.json['client_id']}
        item = get_model(
            self.oauth_colln, query_doc, model_cls=OAuth2AuthorizationCodeModel)  # type: OAuth2AuthorizationCodeModel

        if item and not item.is_expired():
            return item

    def delete_authorization_code(self, authorization_code):
        selector_doc = {"jsonClass": "OAuth2AuthorizationCode", "_id": authorization_code.json['_id']}
        self.oauth_colln.delete_one(selector_doc)

    def authenticate_user(self, authorization_code):
        user = get_model(
            self.users_colln, users_helper.get_user_selector_doc(_id=authorization_code.json['user_id']), UserModel)
        return user


class PasswordGrant(grants.ResourceOwnerPasswordCredentialsGrant):

    def __init__(self, request, server):
        super(PasswordGrant, self).__init__(request, server)
        self.oauth_colln = self.server.oauth_colln
        self.users_colln = self.server.users_colln

    def authenticate_user(self, username, password):
        user = get_model(self.users_colln, users_helper.get_user_selector_doc(email=username), UserModel)
        if user.check_password(password):
            return user


class RefreshTokenGrant(grants.RefreshTokenGrant):

    TOKEN_ENDPOINT_AUTH_METHODS = ['client_secret_post', 'client_secret_basic']

    def __init__(self, request, server):
        super(RefreshTokenGrant, self).__init__(request, server)
        self.oauth_colln = self.server.oauth_colln
        self.users_colln = self.server.users_colln

    def authenticate_refresh_token(self, refresh_token):
        query_doc = {"jsonClass": "OAuth2Token", "refresh_token": refresh_token}
        item = get_model(self.oauth_colln, query_doc, model_cls=OAuth2TokenModel)  # type: OAuth2TokenModel

        if item and not item.is_refresh_token_expired():
            return item

    def authenticate_user(self, credential):
        user = get_model(
            self.users_colln, users_helper.get_user_selector_doc(_id=credential.json['user_id']), UserModel)
        return user


class ClientCredentialsGrant(grants.ClientCredentialsGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = ['client_secret_post', 'client_secret_basic']
    pass
