from flask import g
import flask_restx

from vedavaapi.common.helpers.api_helper import error_response

from . import api

setup_ns = api.namespace('setup', path='/setup', description='setup namespace')


# noinspection PyMethodMayBeStatic
@setup_ns.route('/initial_agents')
class InitialAgents(flask_restx.Resource):

    def get(self):
        return {
            "root_admins_team_id": g.initial_agents.root_admins_team_id,
            "all_users_team_id": g.initial_agents.all_users_team_id
        }

@setup_ns.route('/admin')
class Admin(flask_restx.Resource):

    post_parser = api.parser()
    post_parser.add_argument('email', location='form', type=str, required=True)
    post_parser.add_argument('password', location='form', type=str, required=True)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        from ...agents_helpers import bootstrap_helper
        if not bootstrap_helper.is_root_admins_team_empty(g.current_org_name):
            return error_response(message='root admin already exists', code=403)

        root_admin_id = bootstrap_helper.create_root_admin(g.current_org_name, g.acl_svc, args['email'], args['password'])
        return root_admin_id
