$(function() {

    $('#consent-info-client_id').text(queryParams.get('client_id'));
    $('#consent-info-scopes').text(String(queryParams.get('scope') || '').split(' '));
    $('#consent-form-submit').click(postConsent);
})
