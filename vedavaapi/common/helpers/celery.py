from . import celery_helper


celery = celery_helper.make_celery(
    'vedavaapi.common.helpers.celery:celery',
    [
        'vedavaapi.importer.lib.iiif_importer.worker',
        'vedavaapi.importer.lib.pdf.worker',
        'vedavaapi.tasker.lib.mocker.worker',
    ]
)
