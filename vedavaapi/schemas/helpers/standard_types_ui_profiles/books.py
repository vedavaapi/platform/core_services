from ._utils import get_text


profiles = {}

profiles['Library'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Library", "en"),
                get_text("గ్రంథాలయము", "te"),
                get_text("ग्रन्थाकरः", "sa"),
            ],
            "label_plural": [
                get_text("Libraries", "en"),
                get_text("గ్రంథాలయాలు", "te"),
                get_text("ग्रन्थाकराः", "sa"),
            ]
        },
        "instance": {
            "name": "name",
            "description": "description",
            "img_repr": "logo",
        }
    },
    "nav": {
        "source": [
            {
                "json_class": "ScannedBook",
                "default_expanded": True,
                "label": [
                    get_text("Books", "en"),
                    get_text("పుస్తకాలు", "te"),
                    get_text("ग्रन्थाः", "sa"),
                ]
            },
            {
                "json_class": "Document",
                "default_expanded": False,
                "label": [
                    get_text("Documents", "en"),
                    get_text("పత్రములు", "te"),
                ]
            },
            {
                "json_class": "Library",
                "label": [
                    get_text("Sub libraries", "en"),
                    get_text("ఉప గ్రంథాలయాలు", "te"),
                    get_text("उप-ग्रन्थाकराः", "sa")
                ],
            },
        ],
    },
    "actions": [
        {
            "id": "vihari/actions/create-book",
            "label": [
                get_text("Add book", "en"),
                get_text("పుస్తకాన్ని చేర్చు", "te"),
                get_text("ग्रन्थं योजय", "sa"),
            ],
            "params": {
                "source": "_id",
            },
            "icon": "add-outline",
            "iconType": "iconClass"
        },
        {
            "id": "vihari/actions/import-iiif",
            "label": [
                get_text("Import iiif book", "en"),
                get_text("iiif పుస్తకాన్ని దిగుమతించు", "te"),
                get_text("iiif ग्रन्थं आनय", "sa"),
            ],
            "params": {
                "source": "_id",
            },
            "icon": "iiif.svg",
            "iconType": "url",
        },
        {
            "id": "vihari/actions/import-pdf",
            "label": [
                get_text("Import pdf", "en"),
                get_text("pdf ను దిగుమతించు", "te"),
                get_text("pdf ग्रन्थं आनय", "sa"),
            ],
            "params": {
                "source": "_id",
            },
            "icon": "document",
            "iconType": "iconClass",
        },
        {
            "id": "vihari/actions/bulk-import-pdfs",
            "label": [
                get_text("Bulk import pdfs", "en"),
                get_text("బహుళ pdf లను దిగుమతించు", "te"),
            ],
            "params": {
                "source": "_id",
            },
            "icon": "folder-outline",
            "iconType": "iconClass",
        },
    ],
    "search": {
        "simple": {
            "fields": ["name", "description"],
            "label": [
                get_text("name or description", "en"),
                get_text("పేరు లేదా వివరణ", "te"),
                get_text("नाम, विवरणम् वा", "sa"),
            ],
        },
        "advanced": [
            {
                "field": "name",
                "label": [
                    get_text("Name", "en"),
                    get_text("పేరు", "te"),
                    get_text("नाम", "sa"),
                ],
            },
            {
                "field": "description",
                "label": [
                    get_text("Description", "en"),
                    get_text("వివరణ", "te"),
                    get_text("विवरणम्", "sa"),
                ]
            },
        ],
    },
}


profiles['ScannedBook'] = {
    "repr": {
        "projection": {
            "representations": 0,
        },
        "cls": {
            "label": [
                get_text("Book", "en"),
                get_text("పుస్తకము", "te"),
                get_text("ग्रन्थः", "sa"),
            ],
            "label_plural": [
                get_text("Books", "en"),
                get_text("పుస్తకాలు", "te"),
                get_text("ग्रन्थाः", "sa"),
            ],
        },
        "instance": {
            "name": "title",
            "by": "author",
            "description": "description",
            "img_repr": "cover",
        },
    },
    "nav": {
        "source": [{
            "json_class": "ScannedPage",
            "default_expanded": True,
            "label": [
                get_text("Pages", "en"),
                get_text("పుటలు", "te"),
                get_text("पुटानि", "sa"),
            ]
        }],
        "target": [{
            "json_class": "SequenceAnnotation",
            "label": [
                get_text("Sequences", "en"),
                get_text("సరళులు", "te"),
                get_text("क्रमाः", "sa"),
            ],
        }],
    },
    "actions": [],
    "search": {
        "simple": {
            "fields": ["title", "author"],
            "label": [
                get_text("title or author", "en"),
                get_text("పేరు లేదా కర్త", "te"),
                get_text("शीर्षिका कर्ता वा", "sa"),
            ],
        },
        "advanced": [
            {
                "field": "title",
                "label": [
                    get_text("Title", "en"),
                    get_text("పేరు", "te"),
                    get_text("शीर्षिक", "sa"),
                ],
            },
            {
                "field": "author",
                "label": [
                    get_text("Author", "en"),
                    get_text("గ్రంథకర్త", "te"),
                    get_text("कर्ता", "sa"),
                ],
            },
            {
                "field": "source",
                "label": [
                    get_text("Library", "en"),
                    get_text("గ్రంథాలయము", "te"),
                    get_text("ग्रन्थाकरः", "sa"),
                ],
                "type": "_id",
            },
        ],
    },
}


profiles['ScannedPage'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Page", "en"),
                get_text("పుట", "te"),
                get_text("पुटम्", "sa"),
            ],
            "label_plural": [
                get_text("Pages", "en"),
                get_text("పుటలు", "te"),
                get_text("पुटानि", "sa"),
            ],
        },
        "instance": {
            "img_repr": "representations.stillImage.0",
            "name": "selector"
        },
    },
    "nav": {
        "source": [
            {
                "json_class": "ImageRegion",
                "default_expanded": True,
                "label": [
                    get_text("Image regions", "en"),
                    get_text("చిత్ర ఖండాలు", "te"),
                    get_text("चित्र खण्डाः", "sa"),
                ]
            },
            {
                "json_class": "HOCRPage",
                "label": [
                    get_text("Page layouts", "en"),
                    get_text("పుట ఆకృతులు", "te"),
                    get_text("पुटाकृतयः", "sa"),
                ],
            },
        ],
    },
    "search": {
        "advanced": [
            {
                "field": "source",
                "label": [
                    get_text("Book", "en"),
                    get_text("పుస్తకము", "te"),
                    get_text("ग्रन्थः", "sa"),
                ],
                "type": "_id",
            },
            {
                "field": "selector.index",
                "label": [
                    get_text("Page no?", "en"),
                    get_text("క్రమసంఖ్య", "te"),
                    get_text("पुटसंख्या", "sa"),
                ],
            },
        ],
    },
}


profiles['ImageRegion'] = {
    "repr": {
        "cls": {
            "label": [
                get_text("Image region", "en"),
                get_text("చిత్ర ఖండం", "te"),
                get_text("चित्रखण्डः", "sa"),
            ],
            "label_plural": [
                get_text("Image regions", "en"),
                get_text("చిత్ర ఖండాలు", "te"),
                get_text("चित्रखण्डाः", "sa"),
            ],
        },
        "instance": {
            "img_repr": {
                "type": "field",
                "value": "img_repr",
                "field_of": "pr",
            },
            "img_repr_selector": "selector.default",
            "name": "selector.default"
        },
    },
    "nav": {
        "target": [
            {
                "json_class": "TextAnnotation",
                "default_expanded": True,
                "label": [
                    get_text("Text annotations", "en"),
                    get_text("పాఠ్య ఉల్లేఖనాలు", "te"),
                    get_text("उल्लेखनानि", "sa"),
                ]
            },
        ],
    },
    "search": {
        "advanced": [
            {
                "field": "source",
                "label": [
                    get_text("Source page", "en"),
                    get_text("మూల పుట", "te"),
                    get_text("मूलपुटम्", "sa"),
                ],
                "type": "_id",
            },
        ],
    },
}


profiles["TextDocument"] = {
    "repr": {
        "projection": {
            "content": 0,
            "components": 0,
        },
        "cls": {
            "label": [
                get_text("Text document", "en"),
                get_text("పాఠ్య దస్త్రము", "te"),
                get_text("लिखित-ग्रन्थः", "sa"),
            ],
            "label_plural": [
                get_text("Text documents", "en"),
                get_text("పాఠ్య దస్త్రాలు", "te"),
                get_text("लिखित ग्रन्थाः", "sa"),
            ],
        },
        "instance": {
            "name": "title",
            "description": "description",
            "by": "author",
        },
    },
    "nav": {
        "source": [
            {
                "json_class": "TextSection",
                "default_expanded": False,
            },
        ],
    },
    "actions": [
        {
            "id": "textdocs:edit",
            "params": {
                "textdoc_id": "_id",
            },
        },
    ],
    "search": profiles["ScannedBook"]["search"],
}


profiles["TextSection"] = {
    "repr": {
        "projection": {
            "content": 0,
            "components": 0,
        }
    }
}
