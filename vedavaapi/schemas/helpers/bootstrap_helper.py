from copy import deepcopy

from vedavaapi.acls import AclSvc
from vedavaapi.objectdb.helpers import objstore_graph_helper
from vedavaapi.acls.permissions_helper import PermissionResolver

from .builtin_schemas import vv_builtin_schemas
from vv_schemas import recursively_merge_json_schemas as _mfn, SchemaValidator
from ..helpers import VVSchemas


def _create_schemas_graph(vv_schemas_interface, schemas_graph, ui_profiles=None):
    final_schemas = {}

    def _compute_schema(schema):
        json_class = schema['name']
        source = schema.get('source', None)

        if source:
            if source in final_schemas:
                parent_schema = final_schemas[source]
            elif source in schemas_graph:
                parent_schema = _compute_schema(schemas_graph[source])
            else:
                parent_schema = vv_schemas_interface.get(source)
                if not parent_schema:
                    raise ValueError('schema for {} not found at remote or in given list'.format(source))
        else:
            parent_schema = None

        if parent_schema:
            for k in ('properties', 'required', '_display', 'type', 'jsonClass'):
                if k in schema and k not in parent_schema:
                    continue
                if k not in schema and k in parent_schema:
                    schema[k] = parent_schema[k]
                if k in schema and k in parent_schema:
                    schema[k] = _mfn(parent_schema[k], schema[k], json_path=k)

        # handle META schema self referencing.
        if json_class == 'VVSchema' and isinstance(schema.get('required'), list):
            # schema['required'].clear()
            schema.pop('required')

        final_schemas[json_class] = schema
        # print(computed_schemas)

    for schema in schemas_graph.values():
        if not schema:
            continue
        _compute_schema(deepcopy(schema))

    for schema in final_schemas.values():
        # job1
        if ui_profiles and schema['name'] in ui_profiles:
            schema['_ui_profile'] = ui_profiles[schema['name']]
            schema['_ui_profile']['jsonClass'] = 'UiProfile'

        #job2
        source = schema.get('source', None)
        if source:
            if source in final_schemas:
                schema['source'] = '_:{}'.format(source)
            else:
                parent_schema = vv_schemas_interface.get(source)
                schema['source'] = parent_schema['_id']

    return dict(('_:{}'.format(k), v) for k, v in final_schemas.items())


def get_builtin_schemas_graph():
    return _create_schemas_graph(None, vv_builtin_schemas)


def get_standard_schemas_graph(colln):
    from .standard_schemas import get_schemas
    standard_schemas_graph = get_schemas()
    from .standard_types_ui_profiles import get_profiles
    standard_ui_profiles = get_profiles()
    vv_schemas = VVSchemas(colln)
    return _create_schemas_graph(vv_schemas, standard_schemas_graph, ui_profiles=standard_ui_profiles)


def is_bootstrap_done(colln):
    json_object_def = colln.find_one(
        {"jsonClass": "VVSchema", "name": "JsonObject"}, projection={"_id": 1, "jsonClass": 1})
    return json_object_def is not None


def bootstrap_initial_schemas(colln, acl_svc: AclSvc, initial_agents):
    if is_bootstrap_done(colln):
        return

    graph = get_builtin_schemas_graph()
    graph_ids_to_uids_map, _ = objstore_graph_helper.post_graph_with_ool_data(
        colln, acl_svc, None, None, [], graph, {}, {}, None, validate_schemas=False, validate_permissions=False, upsert=True)

    standard_schemas_graph = get_standard_schemas_graph(colln)
    ss_graph_ids_to_uids_map, _ = objstore_graph_helper.post_graph_with_ool_data(
        colln, acl_svc, None, None, [], standard_schemas_graph, {}, {}, SchemaValidator(VVSchemas(colln), colln),
        validate_schemas=False, validate_permissions=False, upsert=True)

    graph_ids_to_uids_map.update(standard_schemas_graph)

    acl_svc.add_to_agent_ids(
        list(graph_ids_to_uids_map.values()), [PermissionResolver.READ, PermissionResolver.CREATE_CHILDREN],
        PermissionResolver.GRANT, user_ids=[None], team_ids=[initial_agents.all_users_team_id])
    acl_svc.add_to_agent_ids(
        list(graph_ids_to_uids_map.values()), [PermissionResolver.UPDATE_CONTENT],
        PermissionResolver.GRANT, team_ids=[initial_agents.root_admins_team_id])
